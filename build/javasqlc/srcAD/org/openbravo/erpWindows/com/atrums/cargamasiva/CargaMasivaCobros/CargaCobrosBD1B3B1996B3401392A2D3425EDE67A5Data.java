//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.cargamasiva.CargaMasivaCobros;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data implements FieldProvider {
static Logger log4j = Logger.getLogger(CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String numEstab;
  public String logmessage;
  public String ptoEmision;
  public String adOrgId;
  public String isactive;
  public String documentno;
  public String referenceno;
  public String description;
  public String accountingDate;
  public String amount;
  public String depositDate;
  public String cbpartnerName;
  public String finAccountName;
  public String glitemName;
  public String doctypeName;
  public String finMethodName;
  public String procesar;
  public String adClientId;
  public String cmvPaymentId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("num_estab") || fieldName.equals("numEstab"))
      return numEstab;
    else if (fieldName.equalsIgnoreCase("logmessage"))
      return logmessage;
    else if (fieldName.equalsIgnoreCase("pto_emision") || fieldName.equals("ptoEmision"))
      return ptoEmision;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("referenceno"))
      return referenceno;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("accounting_date") || fieldName.equals("accountingDate"))
      return accountingDate;
    else if (fieldName.equalsIgnoreCase("amount"))
      return amount;
    else if (fieldName.equalsIgnoreCase("deposit_date") || fieldName.equals("depositDate"))
      return depositDate;
    else if (fieldName.equalsIgnoreCase("cbpartner_name") || fieldName.equals("cbpartnerName"))
      return cbpartnerName;
    else if (fieldName.equalsIgnoreCase("fin_account_name") || fieldName.equals("finAccountName"))
      return finAccountName;
    else if (fieldName.equalsIgnoreCase("glitem_name") || fieldName.equals("glitemName"))
      return glitemName;
    else if (fieldName.equalsIgnoreCase("doctype_name") || fieldName.equals("doctypeName"))
      return doctypeName;
    else if (fieldName.equalsIgnoreCase("fin_method_name") || fieldName.equals("finMethodName"))
      return finMethodName;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("cmv_payment_id") || fieldName.equals("cmvPaymentId"))
      return cmvPaymentId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(cmv_payment.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = cmv_payment.CreatedBy) as CreatedByR, " +
      "        to_char(cmv_payment.Updated, ?) as updated, " +
      "        to_char(cmv_payment.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        cmv_payment.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = cmv_payment.UpdatedBy) as UpdatedByR," +
      "        cmv_payment.NUM_Estab, " +
      "cmv_payment.Logmessage, " +
      "cmv_payment.PTO_Emision, " +
      "cmv_payment.AD_Org_ID, " +
      "COALESCE(cmv_payment.Isactive, 'N') AS Isactive, " +
      "cmv_payment.Documentno, " +
      "cmv_payment.Referenceno, " +
      "cmv_payment.Description, " +
      "cmv_payment.Accounting_Date, " +
      "cmv_payment.Amount, " +
      "cmv_payment.Deposit_Date, " +
      "cmv_payment.Cbpartner_Name, " +
      "cmv_payment.FIN_Account_Name, " +
      "cmv_payment.Glitem_Name, " +
      "cmv_payment.Doctype_Name, " +
      "cmv_payment.FIN_Method_Name, " +
      "cmv_payment.Procesar, " +
      "cmv_payment.AD_Client_ID, " +
      "cmv_payment.CMV_Payment_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM cmv_payment" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND cmv_payment.CMV_Payment_ID = ? " +
      "        AND cmv_payment.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND cmv_payment.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data = new CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data();
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.created = UtilSql.getValue(result, "created");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.updated = UtilSql.getValue(result, "updated");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.numEstab = UtilSql.getValue(result, "num_estab");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.logmessage = UtilSql.getValue(result, "logmessage");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.ptoEmision = UtilSql.getValue(result, "pto_emision");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.isactive = UtilSql.getValue(result, "isactive");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.documentno = UtilSql.getValue(result, "documentno");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.referenceno = UtilSql.getValue(result, "referenceno");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.description = UtilSql.getValue(result, "description");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.accountingDate = UtilSql.getDateValue(result, "accounting_date", "dd-MM-yyyy");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.amount = UtilSql.getValue(result, "amount");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.depositDate = UtilSql.getDateValue(result, "deposit_date", "dd-MM-yyyy");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.cbpartnerName = UtilSql.getValue(result, "cbpartner_name");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.finAccountName = UtilSql.getValue(result, "fin_account_name");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.glitemName = UtilSql.getValue(result, "glitem_name");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.doctypeName = UtilSql.getValue(result, "doctype_name");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.finMethodName = UtilSql.getValue(result, "fin_method_name");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.procesar = UtilSql.getValue(result, "procesar");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.cmvPaymentId = UtilSql.getValue(result, "cmv_payment_id");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.language = UtilSql.getValue(result, "language");
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.adUserClient = "";
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.adOrgClient = "";
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.createdby = "";
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.trBgcolor = "";
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.totalCount = "";
        objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[] = new CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[vector.size()];
    vector.copyInto(objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data);
    return(objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data);
  }

/**
Create a registry
 */
  public static CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[] set(String doctypeName, String finAccountName, String amount, String cmvPaymentId, String createdby, String createdbyr, String cbpartnerName, String ptoEmision, String adClientId, String numEstab, String adOrgId, String isactive, String description, String procesar, String finMethodName, String referenceno, String documentno, String depositDate, String logmessage, String glitemName, String updatedby, String updatedbyr, String accountingDate)    throws ServletException {
    CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[] = new CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[1];
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0] = new CargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data();
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].created = "";
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].createdbyr = createdbyr;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].updated = "";
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].updatedTimeStamp = "";
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].updatedby = updatedby;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].updatedbyr = updatedbyr;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].numEstab = numEstab;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].logmessage = logmessage;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].ptoEmision = ptoEmision;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].adOrgId = adOrgId;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].isactive = isactive;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].documentno = documentno;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].referenceno = referenceno;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].description = description;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].accountingDate = accountingDate;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].amount = amount;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].depositDate = depositDate;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].cbpartnerName = cbpartnerName;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].finAccountName = finAccountName;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].glitemName = glitemName;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].doctypeName = doctypeName;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].finMethodName = finMethodName;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].procesar = procesar;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].adClientId = adClientId;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].cmvPaymentId = cmvPaymentId;
    objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data[0].language = "";
    return objectCargaCobrosBD1B3B1996B3401392A2D3425EDE67A5Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef4061B074CDAA4F4D803845E153D052DC_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefFB3B961BB63D451DA7BE7E26A4063DF0_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE cmv_payment" +
      "        SET NUM_Estab = (?) , Logmessage = (?) , PTO_Emision = (?) , AD_Org_ID = (?) , Isactive = (?) , Documentno = (?) , Referenceno = (?) , Description = (?) , Accounting_Date = TO_DATE(?) , Amount = TO_NUMBER(?) , Deposit_Date = TO_DATE(?) , Cbpartner_Name = (?) , FIN_Account_Name = (?) , Glitem_Name = (?) , Doctype_Name = (?) , FIN_Method_Name = (?) , Procesar = (?) , AD_Client_ID = (?) , CMV_Payment_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE cmv_payment.CMV_Payment_ID = ? " +
      "        AND cmv_payment.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND cmv_payment.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, logmessage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ptoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referenceno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, accountingDate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, depositDate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpartnerName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finAccountName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, glitemName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, doctypeName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finMethodName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cmvPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cmvPaymentId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO cmv_payment " +
      "        (NUM_Estab, Logmessage, PTO_Emision, AD_Org_ID, Isactive, Documentno, Referenceno, Description, Accounting_Date, Amount, Deposit_Date, Cbpartner_Name, FIN_Account_Name, Glitem_Name, Doctype_Name, FIN_Method_Name, Procesar, AD_Client_ID, CMV_Payment_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_NUMBER(?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, numEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, logmessage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ptoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, referenceno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, accountingDate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, amount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, depositDate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cbpartnerName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finAccountName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, glitemName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, doctypeName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finMethodName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cmvPaymentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM cmv_payment" +
      "        WHERE cmv_payment.CMV_Payment_ID = ? " +
      "        AND cmv_payment.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND cmv_payment.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM cmv_payment" +
      "         WHERE cmv_payment.CMV_Payment_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM cmv_payment" +
      "         WHERE cmv_payment.CMV_Payment_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
