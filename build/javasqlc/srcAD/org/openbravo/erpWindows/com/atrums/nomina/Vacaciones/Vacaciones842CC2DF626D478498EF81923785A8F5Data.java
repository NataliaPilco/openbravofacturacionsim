//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.Vacaciones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Vacaciones842CC2DF626D478498EF81923785A8F5Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Vacaciones842CC2DF626D478498EF81923785A8F5Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String anio;
  public String anior;
  public String vacaciones;
  public String bonificaciones;
  public String saldo;
  public String isactive;
  public String adClientId;
  public String noVacacionId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("anior"))
      return anior;
    else if (fieldName.equalsIgnoreCase("vacaciones"))
      return vacaciones;
    else if (fieldName.equalsIgnoreCase("bonificaciones"))
      return bonificaciones;
    else if (fieldName.equalsIgnoreCase("saldo"))
      return saldo;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_vacacion_id") || fieldName.equals("noVacacionId"))
      return noVacacionId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Vacaciones842CC2DF626D478498EF81923785A8F5Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Vacaciones842CC2DF626D478498EF81923785A8F5Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_vacacion.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_vacacion.CreatedBy) as CreatedByR, " +
      "        to_char(no_vacacion.Updated, ?) as updated, " +
      "        to_char(no_vacacion.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_vacacion.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_vacacion.UpdatedBy) as UpdatedByR," +
      "        no_vacacion.AD_Org_ID, " +
      "(CASE WHEN no_vacacion.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_vacacion.C_Bpartner_ID, " +
      "(CASE WHEN no_vacacion.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "no_vacacion.Anio, " +
      "(CASE WHEN no_vacacion.Anio IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS AnioR, " +
      "no_vacacion.vacaciones, " +
      "no_vacacion.bonificaciones, " +
      "no_vacacion.saldo, " +
      "COALESCE(no_vacacion.Isactive, 'N') AS Isactive, " +
      "no_vacacion.AD_Client_ID, " +
      "no_vacacion.NO_Vacacion_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_vacacion left join (select AD_Org_ID, Name from AD_Org) table1 on (no_vacacion.AD_Org_ID = table1.AD_Org_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (no_vacacion.C_Bpartner_ID = table2.C_BPartner_ID) left join ad_ref_list_v list1 on (no_vacacion.Anio = list1.value and list1.ad_reference_id = 'DBE0CE97929A4EB3A2093E35875EA82B' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_vacacion.NO_Vacacion_ID = ? " +
      "        AND no_vacacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_vacacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Vacaciones842CC2DF626D478498EF81923785A8F5Data objectVacaciones842CC2DF626D478498EF81923785A8F5Data = new Vacaciones842CC2DF626D478498EF81923785A8F5Data();
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.created = UtilSql.getValue(result, "created");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.updated = UtilSql.getValue(result, "updated");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.anio = UtilSql.getValue(result, "anio");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.anior = UtilSql.getValue(result, "anior");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.vacaciones = UtilSql.getValue(result, "vacaciones");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.bonificaciones = UtilSql.getValue(result, "bonificaciones");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.saldo = UtilSql.getValue(result, "saldo");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.isactive = UtilSql.getValue(result, "isactive");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.noVacacionId = UtilSql.getValue(result, "no_vacacion_id");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.language = UtilSql.getValue(result, "language");
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.adUserClient = "";
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.adOrgClient = "";
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.createdby = "";
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.trBgcolor = "";
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.totalCount = "";
        objectVacaciones842CC2DF626D478498EF81923785A8F5Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectVacaciones842CC2DF626D478498EF81923785A8F5Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Vacaciones842CC2DF626D478498EF81923785A8F5Data objectVacaciones842CC2DF626D478498EF81923785A8F5Data[] = new Vacaciones842CC2DF626D478498EF81923785A8F5Data[vector.size()];
    vector.copyInto(objectVacaciones842CC2DF626D478498EF81923785A8F5Data);
    return(objectVacaciones842CC2DF626D478498EF81923785A8F5Data);
  }

/**
Create a registry
 */
  public static Vacaciones842CC2DF626D478498EF81923785A8F5Data[] set(String anio, String createdby, String createdbyr, String bonificaciones, String vacaciones, String isactive, String noVacacionId, String adClientId, String saldo, String adOrgId, String cBpartnerId, String cBpartnerIdr, String updatedby, String updatedbyr)    throws ServletException {
    Vacaciones842CC2DF626D478498EF81923785A8F5Data objectVacaciones842CC2DF626D478498EF81923785A8F5Data[] = new Vacaciones842CC2DF626D478498EF81923785A8F5Data[1];
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0] = new Vacaciones842CC2DF626D478498EF81923785A8F5Data();
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].created = "";
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].createdbyr = createdbyr;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].updated = "";
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].updatedTimeStamp = "";
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].updatedby = updatedby;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].updatedbyr = updatedbyr;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].adOrgId = adOrgId;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].adOrgIdr = "";
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].cBpartnerId = cBpartnerId;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].cBpartnerIdr = cBpartnerIdr;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].anio = anio;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].anior = "";
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].vacaciones = vacaciones;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].bonificaciones = bonificaciones;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].saldo = saldo;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].isactive = isactive;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].adClientId = adClientId;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].noVacacionId = noVacacionId;
    objectVacaciones842CC2DF626D478498EF81923785A8F5Data[0].language = "";
    return objectVacaciones842CC2DF626D478498EF81923785A8F5Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef0909E63254424FA5A6B97C8E9E701409_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefD5C384565A8242D18F698FF65CAE31EF_1(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDE5682896BC94605947571648B7782BF_2(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_vacacion" +
      "        SET AD_Org_ID = (?) , C_Bpartner_ID = (?) , Anio = (?) , vacaciones = TO_NUMBER(?) , bonificaciones = TO_NUMBER(?) , saldo = TO_NUMBER(?) , Isactive = (?) , AD_Client_ID = (?) , NO_Vacacion_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_vacacion.NO_Vacacion_ID = ? " +
      "        AND no_vacacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_vacacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vacaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bonificaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, saldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_vacacion " +
      "        (AD_Org_ID, C_Bpartner_ID, Anio, vacaciones, bonificaciones, saldo, Isactive, AD_Client_ID, NO_Vacacion_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vacaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bonificaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, saldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_vacacion" +
      "        WHERE no_vacacion.NO_Vacacion_ID = ? " +
      "        AND no_vacacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_vacacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_vacacion" +
      "         WHERE no_vacacion.NO_Vacacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_vacacion" +
      "         WHERE no_vacacion.NO_Vacacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
