//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.contabilidad.ProductosT;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Product228117210CAE44B9868C8CA24B810FD6Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Product228117210CAE44B9868C8CA24B810FD6Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String managevariants;
  public String value;
  public String name;
  public String adImageId;
  public String cUomId;
  public String cUomIdr;
  public String mProductCategoryId;
  public String mProductCategoryIdr;
  public String cTaxcategoryId;
  public String cTaxcategoryIdr;
  public String isgeneric;
  public String ispurchased;
  public String issold;
  public String genericProductId;
  public String genericProductIdr;
  public String description;
  public String producttype;
  public String producttyper;
  public String isstocked;
  public String weight;
  public String cUomWeightId;
  public String cUomWeightIdr;
  public String costtype;
  public String costtyper;
  public String coststd;
  public String mAttributesetId;
  public String mAttributesetIdr;
  public String attrsetvaluetype;
  public String attrsetvaluetyper;
  public String mAttributesetinstanceId;
  public String mAttributesetinstanceIdr;
  public String isactive;
  public String upc;
  public String mBrandId;
  public String mBrandIdr;
  public String salesrepId;
  public String cBpartnerId;
  public String imageurl;
  public String descriptionurl;
  public String production;
  public String maProcessplanId;
  public String maProcessplanIdr;
  public String issummary;
  public String mLocatorId;
  public String volume;
  public String shelfwidth;
  public String shelfheight;
  public String shelfdepth;
  public String unitsperpallet;
  public String discontinued;
  public String discontinuedby;
  public String isbom;
  public String isinvoiceprintdetails;
  public String ispicklistprintdetails;
  public String isverified;
  public String processing;
  public String sExpensetypeId;
  public String isquantityvariable;
  public String sResourceId;
  public String isdeferredrevenue;
  public String isdeferredexpense;
  public String bookusingpoprice;
  public String characteristicDesc;
  public String revplantype;
  public String revplantyper;
  public String periodnumber;
  public String defaultperiod;
  public String defaultperiodr;
  public String expplantype;
  public String expplantyper;
  public String periodnumberExp;
  public String defaultperiodExp;
  public String defaultperiodExpr;
  public String calculated;
  public String capacity;
  public String delaymin;
  public String mrpPlannerId;
  public String mrpPlanningmethodId;
  public String qtymax;
  public String qtymin;
  public String qtystd;
  public String qtytype;
  public String stockmin;
  public String createvariants;
  public String updateinvariants;
  public String islinkedtoproduct;
  public String prodCatSelection;
  public String prodCatSelectionr;
  public String productSelection;
  public String productSelectionr;
  public String returnable;
  public String overdueReturnDays;
  public String ispricerulebased;
  public String quantityRule;
  public String quantityRuler;
  public String uniquePerDocument;
  public String printDescription;
  public String relateprodcattoservice;
  public String relateprodtoservice;
  public String allowDeferredSell;
  public String deferredSellMaxDays;
  public String versionno;
  public String guaranteedays;
  public String mFreightcategoryId;
  public String enforceAttribute;
  public String adClientId;
  public String help;
  public String classification;
  public String stockMin;
  public String name2;
  public String downloadurl;
  public String ispriceprinted;
  public String documentnote;
  public String mProductId;
  public String sku;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("managevariants"))
      return managevariants;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("ad_image_id") || fieldName.equals("adImageId"))
      return adImageId;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("c_uom_idr") || fieldName.equals("cUomIdr"))
      return cUomIdr;
    else if (fieldName.equalsIgnoreCase("m_product_category_id") || fieldName.equals("mProductCategoryId"))
      return mProductCategoryId;
    else if (fieldName.equalsIgnoreCase("m_product_category_idr") || fieldName.equals("mProductCategoryIdr"))
      return mProductCategoryIdr;
    else if (fieldName.equalsIgnoreCase("c_taxcategory_id") || fieldName.equals("cTaxcategoryId"))
      return cTaxcategoryId;
    else if (fieldName.equalsIgnoreCase("c_taxcategory_idr") || fieldName.equals("cTaxcategoryIdr"))
      return cTaxcategoryIdr;
    else if (fieldName.equalsIgnoreCase("isgeneric"))
      return isgeneric;
    else if (fieldName.equalsIgnoreCase("ispurchased"))
      return ispurchased;
    else if (fieldName.equalsIgnoreCase("issold"))
      return issold;
    else if (fieldName.equalsIgnoreCase("generic_product_id") || fieldName.equals("genericProductId"))
      return genericProductId;
    else if (fieldName.equalsIgnoreCase("generic_product_idr") || fieldName.equals("genericProductIdr"))
      return genericProductIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("producttype"))
      return producttype;
    else if (fieldName.equalsIgnoreCase("producttyper"))
      return producttyper;
    else if (fieldName.equalsIgnoreCase("isstocked"))
      return isstocked;
    else if (fieldName.equalsIgnoreCase("weight"))
      return weight;
    else if (fieldName.equalsIgnoreCase("c_uom_weight_id") || fieldName.equals("cUomWeightId"))
      return cUomWeightId;
    else if (fieldName.equalsIgnoreCase("c_uom_weight_idr") || fieldName.equals("cUomWeightIdr"))
      return cUomWeightIdr;
    else if (fieldName.equalsIgnoreCase("costtype"))
      return costtype;
    else if (fieldName.equalsIgnoreCase("costtyper"))
      return costtyper;
    else if (fieldName.equalsIgnoreCase("coststd"))
      return coststd;
    else if (fieldName.equalsIgnoreCase("m_attributeset_id") || fieldName.equals("mAttributesetId"))
      return mAttributesetId;
    else if (fieldName.equalsIgnoreCase("m_attributeset_idr") || fieldName.equals("mAttributesetIdr"))
      return mAttributesetIdr;
    else if (fieldName.equalsIgnoreCase("attrsetvaluetype"))
      return attrsetvaluetype;
    else if (fieldName.equalsIgnoreCase("attrsetvaluetyper"))
      return attrsetvaluetyper;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_id") || fieldName.equals("mAttributesetinstanceId"))
      return mAttributesetinstanceId;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_idr") || fieldName.equals("mAttributesetinstanceIdr"))
      return mAttributesetinstanceIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("upc"))
      return upc;
    else if (fieldName.equalsIgnoreCase("m_brand_id") || fieldName.equals("mBrandId"))
      return mBrandId;
    else if (fieldName.equalsIgnoreCase("m_brand_idr") || fieldName.equals("mBrandIdr"))
      return mBrandIdr;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("imageurl"))
      return imageurl;
    else if (fieldName.equalsIgnoreCase("descriptionurl"))
      return descriptionurl;
    else if (fieldName.equalsIgnoreCase("production"))
      return production;
    else if (fieldName.equalsIgnoreCase("ma_processplan_id") || fieldName.equals("maProcessplanId"))
      return maProcessplanId;
    else if (fieldName.equalsIgnoreCase("ma_processplan_idr") || fieldName.equals("maProcessplanIdr"))
      return maProcessplanIdr;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("m_locator_id") || fieldName.equals("mLocatorId"))
      return mLocatorId;
    else if (fieldName.equalsIgnoreCase("volume"))
      return volume;
    else if (fieldName.equalsIgnoreCase("shelfwidth"))
      return shelfwidth;
    else if (fieldName.equalsIgnoreCase("shelfheight"))
      return shelfheight;
    else if (fieldName.equalsIgnoreCase("shelfdepth"))
      return shelfdepth;
    else if (fieldName.equalsIgnoreCase("unitsperpallet"))
      return unitsperpallet;
    else if (fieldName.equalsIgnoreCase("discontinued"))
      return discontinued;
    else if (fieldName.equalsIgnoreCase("discontinuedby"))
      return discontinuedby;
    else if (fieldName.equalsIgnoreCase("isbom"))
      return isbom;
    else if (fieldName.equalsIgnoreCase("isinvoiceprintdetails"))
      return isinvoiceprintdetails;
    else if (fieldName.equalsIgnoreCase("ispicklistprintdetails"))
      return ispicklistprintdetails;
    else if (fieldName.equalsIgnoreCase("isverified"))
      return isverified;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("s_expensetype_id") || fieldName.equals("sExpensetypeId"))
      return sExpensetypeId;
    else if (fieldName.equalsIgnoreCase("isquantityvariable"))
      return isquantityvariable;
    else if (fieldName.equalsIgnoreCase("s_resource_id") || fieldName.equals("sResourceId"))
      return sResourceId;
    else if (fieldName.equalsIgnoreCase("isdeferredrevenue"))
      return isdeferredrevenue;
    else if (fieldName.equalsIgnoreCase("isdeferredexpense"))
      return isdeferredexpense;
    else if (fieldName.equalsIgnoreCase("bookusingpoprice"))
      return bookusingpoprice;
    else if (fieldName.equalsIgnoreCase("characteristic_desc") || fieldName.equals("characteristicDesc"))
      return characteristicDesc;
    else if (fieldName.equalsIgnoreCase("revplantype"))
      return revplantype;
    else if (fieldName.equalsIgnoreCase("revplantyper"))
      return revplantyper;
    else if (fieldName.equalsIgnoreCase("periodnumber"))
      return periodnumber;
    else if (fieldName.equalsIgnoreCase("defaultperiod"))
      return defaultperiod;
    else if (fieldName.equalsIgnoreCase("defaultperiodr"))
      return defaultperiodr;
    else if (fieldName.equalsIgnoreCase("expplantype"))
      return expplantype;
    else if (fieldName.equalsIgnoreCase("expplantyper"))
      return expplantyper;
    else if (fieldName.equalsIgnoreCase("periodnumber_exp") || fieldName.equals("periodnumberExp"))
      return periodnumberExp;
    else if (fieldName.equalsIgnoreCase("defaultperiod_exp") || fieldName.equals("defaultperiodExp"))
      return defaultperiodExp;
    else if (fieldName.equalsIgnoreCase("defaultperiod_expr") || fieldName.equals("defaultperiodExpr"))
      return defaultperiodExpr;
    else if (fieldName.equalsIgnoreCase("calculated"))
      return calculated;
    else if (fieldName.equalsIgnoreCase("capacity"))
      return capacity;
    else if (fieldName.equalsIgnoreCase("delaymin"))
      return delaymin;
    else if (fieldName.equalsIgnoreCase("mrp_planner_id") || fieldName.equals("mrpPlannerId"))
      return mrpPlannerId;
    else if (fieldName.equalsIgnoreCase("mrp_planningmethod_id") || fieldName.equals("mrpPlanningmethodId"))
      return mrpPlanningmethodId;
    else if (fieldName.equalsIgnoreCase("qtymax"))
      return qtymax;
    else if (fieldName.equalsIgnoreCase("qtymin"))
      return qtymin;
    else if (fieldName.equalsIgnoreCase("qtystd"))
      return qtystd;
    else if (fieldName.equalsIgnoreCase("qtytype"))
      return qtytype;
    else if (fieldName.equalsIgnoreCase("stockmin"))
      return stockmin;
    else if (fieldName.equalsIgnoreCase("createvariants"))
      return createvariants;
    else if (fieldName.equalsIgnoreCase("updateinvariants"))
      return updateinvariants;
    else if (fieldName.equalsIgnoreCase("islinkedtoproduct"))
      return islinkedtoproduct;
    else if (fieldName.equalsIgnoreCase("prod_cat_selection") || fieldName.equals("prodCatSelection"))
      return prodCatSelection;
    else if (fieldName.equalsIgnoreCase("prod_cat_selectionr") || fieldName.equals("prodCatSelectionr"))
      return prodCatSelectionr;
    else if (fieldName.equalsIgnoreCase("product_selection") || fieldName.equals("productSelection"))
      return productSelection;
    else if (fieldName.equalsIgnoreCase("product_selectionr") || fieldName.equals("productSelectionr"))
      return productSelectionr;
    else if (fieldName.equalsIgnoreCase("returnable"))
      return returnable;
    else if (fieldName.equalsIgnoreCase("overdue_return_days") || fieldName.equals("overdueReturnDays"))
      return overdueReturnDays;
    else if (fieldName.equalsIgnoreCase("ispricerulebased"))
      return ispricerulebased;
    else if (fieldName.equalsIgnoreCase("quantity_rule") || fieldName.equals("quantityRule"))
      return quantityRule;
    else if (fieldName.equalsIgnoreCase("quantity_ruler") || fieldName.equals("quantityRuler"))
      return quantityRuler;
    else if (fieldName.equalsIgnoreCase("unique_per_document") || fieldName.equals("uniquePerDocument"))
      return uniquePerDocument;
    else if (fieldName.equalsIgnoreCase("print_description") || fieldName.equals("printDescription"))
      return printDescription;
    else if (fieldName.equalsIgnoreCase("relateprodcattoservice"))
      return relateprodcattoservice;
    else if (fieldName.equalsIgnoreCase("relateprodtoservice"))
      return relateprodtoservice;
    else if (fieldName.equalsIgnoreCase("allow_deferred_sell") || fieldName.equals("allowDeferredSell"))
      return allowDeferredSell;
    else if (fieldName.equalsIgnoreCase("deferred_sell_max_days") || fieldName.equals("deferredSellMaxDays"))
      return deferredSellMaxDays;
    else if (fieldName.equalsIgnoreCase("versionno"))
      return versionno;
    else if (fieldName.equalsIgnoreCase("guaranteedays"))
      return guaranteedays;
    else if (fieldName.equalsIgnoreCase("m_freightcategory_id") || fieldName.equals("mFreightcategoryId"))
      return mFreightcategoryId;
    else if (fieldName.equalsIgnoreCase("enforce_attribute") || fieldName.equals("enforceAttribute"))
      return enforceAttribute;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("help"))
      return help;
    else if (fieldName.equalsIgnoreCase("classification"))
      return classification;
    else if (fieldName.equalsIgnoreCase("stock_min") || fieldName.equals("stockMin"))
      return stockMin;
    else if (fieldName.equalsIgnoreCase("name2"))
      return name2;
    else if (fieldName.equalsIgnoreCase("downloadurl"))
      return downloadurl;
    else if (fieldName.equalsIgnoreCase("ispriceprinted"))
      return ispriceprinted;
    else if (fieldName.equalsIgnoreCase("documentnote"))
      return documentnote;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("sku"))
      return sku;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Product228117210CAE44B9868C8CA24B810FD6Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Product228117210CAE44B9868C8CA24B810FD6Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Product.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.CreatedBy) as CreatedByR, " +
      "        to_char(M_Product.Updated, ?) as updated, " +
      "        to_char(M_Product.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Product.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.UpdatedBy) as UpdatedByR," +
      "        M_Product.AD_Org_ID, " +
      "(CASE WHEN M_Product.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "M_Product.ManageVariants, " +
      "M_Product.Value, " +
      "M_Product.Name, " +
      "M_Product.AD_Image_ID, " +
      "M_Product.C_UOM_ID, " +
      "(CASE WHEN M_Product.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "M_Product.M_Product_Category_ID, " +
      "(CASE WHEN M_Product.M_Product_Category_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL4.Name IS NULL THEN TO_CHAR(table4.Name) ELSE TO_CHAR(tableTRL4.Name) END)), ''))),'') ) END) AS M_Product_Category_IDR, " +
      "M_Product.C_TaxCategory_ID, " +
      "(CASE WHEN M_Product.C_TaxCategory_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL6.Name IS NULL THEN TO_CHAR(table6.Name) ELSE TO_CHAR(tableTRL6.Name) END)), ''))),'') ) END) AS C_TaxCategory_IDR, " +
      "COALESCE(M_Product.IsGeneric, 'N') AS IsGeneric, " +
      "COALESCE(M_Product.IsPurchased, 'N') AS IsPurchased, " +
      "COALESCE(M_Product.IsSold, 'N') AS IsSold, " +
      "M_Product.Generic_Product_ID, " +
      "(CASE WHEN M_Product.Generic_Product_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL8.Name IS NULL THEN TO_CHAR(table8.Name) ELSE TO_CHAR(tableTRL8.Name) END)), ''))),'') ) END) AS Generic_Product_IDR, " +
      "M_Product.Description, " +
      "M_Product.ProductType, " +
      "(CASE WHEN M_Product.ProductType IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ProductTypeR, " +
      "COALESCE(M_Product.IsStocked, 'N') AS IsStocked, " +
      "M_Product.Weight, " +
      "M_Product.C_Uom_Weight_ID, " +
      "(CASE WHEN M_Product.C_Uom_Weight_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL10.Name IS NULL THEN TO_CHAR(table10.Name) ELSE TO_CHAR(tableTRL10.Name) END)), ''))),'') ) END) AS C_Uom_Weight_IDR, " +
      "M_Product.Costtype, " +
      "(CASE WHEN M_Product.Costtype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS CosttypeR, " +
      "M_Product.Coststd, " +
      "M_Product.M_AttributeSet_ID, " +
      "(CASE WHEN M_Product.M_AttributeSet_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.Name), ''))),'') ) END) AS M_AttributeSet_IDR, " +
      "M_Product.Attrsetvaluetype, " +
      "(CASE WHEN M_Product.Attrsetvaluetype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS AttrsetvaluetypeR, " +
      "M_Product.M_AttributeSetInstance_ID, " +
      "(CASE WHEN M_Product.M_AttributeSetInstance_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Description), ''))),'') ) END) AS M_AttributeSetInstance_IDR, " +
      "COALESCE(M_Product.IsActive, 'N') AS IsActive, " +
      "M_Product.UPC, " +
      "M_Product.M_Brand_ID, " +
      "(CASE WHEN M_Product.M_Brand_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table14.Name), ''))),'') ) END) AS M_Brand_IDR, " +
      "M_Product.SalesRep_ID, " +
      "M_Product.C_BPartner_ID, " +
      "M_Product.ImageURL, " +
      "M_Product.DescriptionURL, " +
      "COALESCE(M_Product.Production, 'N') AS Production, " +
      "M_Product.MA_Processplan_ID, " +
      "(CASE WHEN M_Product.MA_Processplan_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table15.Name), ''))),'') ) END) AS MA_Processplan_IDR, " +
      "COALESCE(M_Product.IsSummary, 'N') AS IsSummary, " +
      "M_Product.M_Locator_ID, " +
      "M_Product.Volume, " +
      "M_Product.ShelfWidth, " +
      "M_Product.ShelfHeight, " +
      "M_Product.ShelfDepth, " +
      "M_Product.UnitsPerPallet, " +
      "COALESCE(M_Product.Discontinued, 'N') AS Discontinued, " +
      "M_Product.DiscontinuedBy, " +
      "COALESCE(M_Product.IsBOM, 'N') AS IsBOM, " +
      "COALESCE(M_Product.IsInvoicePrintDetails, 'N') AS IsInvoicePrintDetails, " +
      "COALESCE(M_Product.IsPickListPrintDetails, 'N') AS IsPickListPrintDetails, " +
      "COALESCE(M_Product.IsVerified, 'N') AS IsVerified, " +
      "M_Product.Processing, " +
      "M_Product.S_ExpenseType_ID, " +
      "COALESCE(M_Product.Isquantityvariable, 'N') AS Isquantityvariable, " +
      "M_Product.S_Resource_ID, " +
      "COALESCE(M_Product.Isdeferredrevenue, 'N') AS Isdeferredrevenue, " +
      "COALESCE(M_Product.Isdeferredexpense, 'N') AS Isdeferredexpense, " +
      "COALESCE(M_Product.Bookusingpoprice, 'N') AS Bookusingpoprice, " +
      "M_Product.Characteristic_Desc, " +
      "M_Product.Revplantype, " +
      "(CASE WHEN M_Product.Revplantype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list4.name),'') ) END) AS RevplantypeR, " +
      "M_Product.Periodnumber, " +
      "M_Product.DefaultPeriod, " +
      "(CASE WHEN M_Product.DefaultPeriod IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list5.name),'') ) END) AS DefaultPeriodR, " +
      "M_Product.Expplantype, " +
      "(CASE WHEN M_Product.Expplantype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list6.name),'') ) END) AS ExpplantypeR, " +
      "M_Product.Periodnumber_Exp, " +
      "M_Product.DefaultPeriod_Exp, " +
      "(CASE WHEN M_Product.DefaultPeriod_Exp IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list7.name),'') ) END) AS DefaultPeriod_ExpR, " +
      "COALESCE(M_Product.Calculated, 'N') AS Calculated, " +
      "M_Product.Capacity, " +
      "M_Product.Delaymin, " +
      "M_Product.MRP_Planner_ID, " +
      "M_Product.MRP_Planningmethod_ID, " +
      "M_Product.Qtymax, " +
      "M_Product.Qtymin, " +
      "M_Product.Qtystd, " +
      "COALESCE(M_Product.Qtytype, 'N') AS Qtytype, " +
      "M_Product.Stockmin, " +
      "M_Product.CreateVariants, " +
      "M_Product.Updateinvariants, " +
      "COALESCE(M_Product.Islinkedtoproduct, 'N') AS Islinkedtoproduct, " +
      "M_Product.Prod_Cat_Selection, " +
      "(CASE WHEN M_Product.Prod_Cat_Selection IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list8.name),'') ) END) AS Prod_Cat_SelectionR, " +
      "M_Product.Product_Selection, " +
      "(CASE WHEN M_Product.Product_Selection IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list9.name),'') ) END) AS Product_SelectionR, " +
      "COALESCE(M_Product.Returnable, 'N') AS Returnable, " +
      "M_Product.Overdue_Return_Days, " +
      "COALESCE(M_Product.Ispricerulebased, 'N') AS Ispricerulebased, " +
      "M_Product.Quantity_Rule, " +
      "(CASE WHEN M_Product.Quantity_Rule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list10.name),'') ) END) AS Quantity_RuleR, " +
      "COALESCE(M_Product.Unique_Per_Document, 'N') AS Unique_Per_Document, " +
      "COALESCE(M_Product.Print_Description, 'N') AS Print_Description, " +
      "M_Product.Relateprodcattoservice, " +
      "M_Product.Relateprodtoservice, " +
      "COALESCE(M_Product.Allow_Deferred_Sell, 'N') AS Allow_Deferred_Sell, " +
      "M_Product.Deferred_Sell_Max_Days, " +
      "M_Product.VersionNo, " +
      "M_Product.GuaranteeDays, " +
      "M_Product.M_FreightCategory_ID, " +
      "COALESCE(M_Product.Enforce_Attribute, 'N') AS Enforce_Attribute, " +
      "M_Product.AD_Client_ID, " +
      "M_Product.Help, " +
      "M_Product.Classification, " +
      "M_Product.Stock_Min, " +
      "M_Product.Name2, " +
      "M_Product.DownloadURL, " +
      "COALESCE(M_Product.Ispriceprinted, 'N') AS Ispriceprinted, " +
      "M_Product.DocumentNote, " +
      "M_Product.M_Product_ID, " +
      "M_Product.SKU, " +
      "        ? AS LANGUAGE " +
      "        FROM M_Product left join (select AD_Org_ID, Name from AD_Org) table1 on (M_Product.AD_Org_ID = table1.AD_Org_ID) left join (select C_UOM_ID, Name from C_UOM) table2 on (M_Product.C_UOM_ID = table2.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL2 on (table2.C_UOM_ID = tableTRL2.C_UOM_ID and tableTRL2.AD_Language = ?)  left join (select M_Product_Category_ID, Name from M_Product_Category) table4 on (M_Product.M_Product_Category_ID = table4.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL4 on (table4.M_Product_Category_ID = tableTRL4.M_Product_Category_ID and tableTRL4.AD_Language = ?)  left join (select C_TaxCategory_ID, Name from C_TaxCategory) table6 on (M_Product.C_TaxCategory_ID = table6.C_TaxCategory_ID) left join (select C_TaxCategory_ID,AD_Language, Name from C_TaxCategory_TRL) tableTRL6 on (table6.C_TaxCategory_ID = tableTRL6.C_TaxCategory_ID and tableTRL6.AD_Language = ?)  left join (select M_Product_ID, Name from M_Product) table8 on (M_Product.Generic_Product_ID =  table8.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL8 on (table8.M_Product_ID = tableTRL8.M_Product_ID and tableTRL8.AD_Language = ?)  left join ad_ref_list_v list1 on (M_Product.ProductType = list1.value and list1.ad_reference_id = '270' and list1.ad_language = ?)  left join (select C_UOM_ID, Name from C_UOM) table10 on (M_Product.C_Uom_Weight_ID =  table10.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL10 on (table10.C_UOM_ID = tableTRL10.C_UOM_ID and tableTRL10.AD_Language = ?)  left join ad_ref_list_v list2 on (M_Product.Costtype = list2.value and list2.ad_reference_id = '800025' and list2.ad_language = ?)  left join (select M_AttributeSet_ID, Name from M_AttributeSet) table12 on (M_Product.M_AttributeSet_ID = table12.M_AttributeSet_ID) left join ad_ref_list_v list3 on (M_Product.Attrsetvaluetype = list3.value and list3.ad_reference_id = '5AD08D5DF85549E0BCC0DEBDE4C0D340' and list3.ad_language = ?)  left join (select M_AttributeSetInstance_ID, Description from M_AttributeSetInstance) table13 on (M_Product.M_AttributeSetInstance_ID = table13.M_AttributeSetInstance_ID) left join (select M_Brand_ID, Name from M_Brand) table14 on (M_Product.M_Brand_ID = table14.M_Brand_ID) left join (select MA_Processplan_ID, Name from MA_Processplan) table15 on (M_Product.MA_Processplan_ID = table15.MA_Processplan_ID) left join ad_ref_list_v list4 on (M_Product.Revplantype = list4.value and list4.ad_reference_id = '73625A8F22EF4CD7808603156BA606D7' and list4.ad_language = ?)  left join ad_ref_list_v list5 on (M_Product.DefaultPeriod = list5.value and list5.ad_reference_id = '6669508E338F4A10BA3E0D241D133E62' and list5.ad_language = ?)  left join ad_ref_list_v list6 on (M_Product.Expplantype = list6.value and list6.ad_reference_id = '73625A8F22EF4CD7808603156BA606D7' and list6.ad_language = ?)  left join ad_ref_list_v list7 on (M_Product.DefaultPeriod_Exp = list7.value and list7.ad_reference_id = '6669508E338F4A10BA3E0D241D133E62' and list7.ad_language = ?)  left join ad_ref_list_v list8 on (M_Product.Prod_Cat_Selection = list8.value and list8.ad_reference_id = '800029' and list8.ad_language = ?)  left join ad_ref_list_v list9 on (M_Product.Product_Selection = list9.value and list9.ad_reference_id = '800029' and list9.ad_language = ?)  left join ad_ref_list_v list10 on (M_Product.Quantity_Rule = list10.value and list10.ad_reference_id = '4E07601C34764669B75FCA1808F55B57' and list10.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Product228117210CAE44B9868C8CA24B810FD6Data objectProduct228117210CAE44B9868C8CA24B810FD6Data = new Product228117210CAE44B9868C8CA24B810FD6Data();
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.created = UtilSql.getValue(result, "created");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updated = UtilSql.getValue(result, "updated");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.managevariants = UtilSql.getValue(result, "managevariants");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.value = UtilSql.getValue(result, "value");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.name = UtilSql.getValue(result, "name");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adImageId = UtilSql.getValue(result, "ad_image_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mProductCategoryId = UtilSql.getValue(result, "m_product_category_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mProductCategoryIdr = UtilSql.getValue(result, "m_product_category_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cTaxcategoryId = UtilSql.getValue(result, "c_taxcategory_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cTaxcategoryIdr = UtilSql.getValue(result, "c_taxcategory_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isgeneric = UtilSql.getValue(result, "isgeneric");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispurchased = UtilSql.getValue(result, "ispurchased");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.issold = UtilSql.getValue(result, "issold");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.genericProductId = UtilSql.getValue(result, "generic_product_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.genericProductIdr = UtilSql.getValue(result, "generic_product_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.description = UtilSql.getValue(result, "description");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.producttype = UtilSql.getValue(result, "producttype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.producttyper = UtilSql.getValue(result, "producttyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isstocked = UtilSql.getValue(result, "isstocked");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.weight = UtilSql.getValue(result, "weight");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomWeightId = UtilSql.getValue(result, "c_uom_weight_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomWeightIdr = UtilSql.getValue(result, "c_uom_weight_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.costtype = UtilSql.getValue(result, "costtype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.costtyper = UtilSql.getValue(result, "costtyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.coststd = UtilSql.getValue(result, "coststd");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetId = UtilSql.getValue(result, "m_attributeset_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetIdr = UtilSql.getValue(result, "m_attributeset_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.attrsetvaluetype = UtilSql.getValue(result, "attrsetvaluetype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.attrsetvaluetyper = UtilSql.getValue(result, "attrsetvaluetyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetinstanceIdr = UtilSql.getValue(result, "m_attributesetinstance_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isactive = UtilSql.getValue(result, "isactive");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.upc = UtilSql.getValue(result, "upc");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mBrandId = UtilSql.getValue(result, "m_brand_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mBrandIdr = UtilSql.getValue(result, "m_brand_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.imageurl = UtilSql.getValue(result, "imageurl");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.descriptionurl = UtilSql.getValue(result, "descriptionurl");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.production = UtilSql.getValue(result, "production");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.maProcessplanId = UtilSql.getValue(result, "ma_processplan_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.maProcessplanIdr = UtilSql.getValue(result, "ma_processplan_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.issummary = UtilSql.getValue(result, "issummary");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.volume = UtilSql.getValue(result, "volume");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.shelfwidth = UtilSql.getValue(result, "shelfwidth");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.shelfheight = UtilSql.getValue(result, "shelfheight");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.shelfdepth = UtilSql.getValue(result, "shelfdepth");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.unitsperpallet = UtilSql.getValue(result, "unitsperpallet");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.discontinued = UtilSql.getValue(result, "discontinued");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.discontinuedby = UtilSql.getDateValue(result, "discontinuedby", "dd-MM-yyyy");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isbom = UtilSql.getValue(result, "isbom");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isinvoiceprintdetails = UtilSql.getValue(result, "isinvoiceprintdetails");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispicklistprintdetails = UtilSql.getValue(result, "ispicklistprintdetails");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isverified = UtilSql.getValue(result, "isverified");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.processing = UtilSql.getValue(result, "processing");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.sExpensetypeId = UtilSql.getValue(result, "s_expensetype_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isquantityvariable = UtilSql.getValue(result, "isquantityvariable");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.sResourceId = UtilSql.getValue(result, "s_resource_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isdeferredrevenue = UtilSql.getValue(result, "isdeferredrevenue");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isdeferredexpense = UtilSql.getValue(result, "isdeferredexpense");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.bookusingpoprice = UtilSql.getValue(result, "bookusingpoprice");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.characteristicDesc = UtilSql.getValue(result, "characteristic_desc");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.revplantype = UtilSql.getValue(result, "revplantype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.revplantyper = UtilSql.getValue(result, "revplantyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.periodnumber = UtilSql.getValue(result, "periodnumber");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiod = UtilSql.getValue(result, "defaultperiod");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiodr = UtilSql.getValue(result, "defaultperiodr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.expplantype = UtilSql.getValue(result, "expplantype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.expplantyper = UtilSql.getValue(result, "expplantyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.periodnumberExp = UtilSql.getValue(result, "periodnumber_exp");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiodExp = UtilSql.getValue(result, "defaultperiod_exp");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiodExpr = UtilSql.getValue(result, "defaultperiod_expr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.calculated = UtilSql.getValue(result, "calculated");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.capacity = UtilSql.getValue(result, "capacity");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.delaymin = UtilSql.getValue(result, "delaymin");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mrpPlannerId = UtilSql.getValue(result, "mrp_planner_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mrpPlanningmethodId = UtilSql.getValue(result, "mrp_planningmethod_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtymax = UtilSql.getValue(result, "qtymax");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtymin = UtilSql.getValue(result, "qtymin");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtystd = UtilSql.getValue(result, "qtystd");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtytype = UtilSql.getValue(result, "qtytype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.stockmin = UtilSql.getValue(result, "stockmin");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.createvariants = UtilSql.getValue(result, "createvariants");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updateinvariants = UtilSql.getValue(result, "updateinvariants");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.islinkedtoproduct = UtilSql.getValue(result, "islinkedtoproduct");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.prodCatSelection = UtilSql.getValue(result, "prod_cat_selection");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.prodCatSelectionr = UtilSql.getValue(result, "prod_cat_selectionr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.productSelection = UtilSql.getValue(result, "product_selection");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.productSelectionr = UtilSql.getValue(result, "product_selectionr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.returnable = UtilSql.getValue(result, "returnable");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispricerulebased = UtilSql.getValue(result, "ispricerulebased");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.quantityRule = UtilSql.getValue(result, "quantity_rule");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.quantityRuler = UtilSql.getValue(result, "quantity_ruler");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.uniquePerDocument = UtilSql.getValue(result, "unique_per_document");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.printDescription = UtilSql.getValue(result, "print_description");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.relateprodcattoservice = UtilSql.getValue(result, "relateprodcattoservice");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.relateprodtoservice = UtilSql.getValue(result, "relateprodtoservice");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.allowDeferredSell = UtilSql.getValue(result, "allow_deferred_sell");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.deferredSellMaxDays = UtilSql.getValue(result, "deferred_sell_max_days");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.versionno = UtilSql.getValue(result, "versionno");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.guaranteedays = UtilSql.getValue(result, "guaranteedays");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mFreightcategoryId = UtilSql.getValue(result, "m_freightcategory_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.enforceAttribute = UtilSql.getValue(result, "enforce_attribute");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.help = UtilSql.getValue(result, "help");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.classification = UtilSql.getValue(result, "classification");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.stockMin = UtilSql.getValue(result, "stock_min");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.name2 = UtilSql.getValue(result, "name2");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.downloadurl = UtilSql.getValue(result, "downloadurl");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispriceprinted = UtilSql.getValue(result, "ispriceprinted");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.documentnote = UtilSql.getValue(result, "documentnote");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mProductId = UtilSql.getValue(result, "m_product_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.sku = UtilSql.getValue(result, "sku");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.language = UtilSql.getValue(result, "language");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adUserClient = "";
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adOrgClient = "";
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.createdby = "";
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.trBgcolor = "";
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.totalCount = "";
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProduct228117210CAE44B9868C8CA24B810FD6Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Product228117210CAE44B9868C8CA24B810FD6Data objectProduct228117210CAE44B9868C8CA24B810FD6Data[] = new Product228117210CAE44B9868C8CA24B810FD6Data[vector.size()];
    vector.copyInto(objectProduct228117210CAE44B9868C8CA24B810FD6Data);
    return(objectProduct228117210CAE44B9868C8CA24B810FD6Data);
  }

/**
Select for relation
 */
  public static Product228117210CAE44B9868C8CA24B810FD6Data[] select(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String parValue, String parName, String parM_Product_Category_ID, String parProductType, String parC_TaxCategory_ID, String parSessionDate, String parSessionUser, String adUserClient, String adOrgClient, String orderbyclause)    throws ServletException {
    return select(connectionProvider, dateTimeFormat, paramLanguage, parValue, parName, parM_Product_Category_ID, parProductType, parC_TaxCategory_ID, parSessionDate, parSessionUser, adUserClient, adOrgClient, orderbyclause, 0, 0);
  }

/**
Select for relation
 */
  public static Product228117210CAE44B9868C8CA24B810FD6Data[] select(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String parValue, String parName, String parM_Product_Category_ID, String parProductType, String parC_TaxCategory_ID, String parSessionDate, String parSessionUser, String adUserClient, String adOrgClient, String orderbyclause, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Product.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.CreatedBy) as CreatedByR, " +
      "        to_char(M_Product.Updated, ?) as updated, " +
      "        to_char(M_Product.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Product.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Product.UpdatedBy) as UpdatedByR, " +
      "        M_Product.AD_Org_ID, " +
      "(CASE WHEN M_Product.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "M_Product.ManageVariants, " +
      "M_Product.Value, " +
      "M_Product.Name, " +
      "M_Product.AD_Image_ID, " +
      "M_Product.C_UOM_ID, " +
      "(CASE WHEN M_Product.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "M_Product.M_Product_Category_ID, " +
      "(CASE WHEN M_Product.M_Product_Category_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL4.Name IS NULL THEN TO_CHAR(table4.Name) ELSE TO_CHAR(tableTRL4.Name) END)), ''))),'') ) END) AS M_Product_Category_IDR, " +
      "M_Product.C_TaxCategory_ID, " +
      "(CASE WHEN M_Product.C_TaxCategory_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL6.Name IS NULL THEN TO_CHAR(table6.Name) ELSE TO_CHAR(tableTRL6.Name) END)), ''))),'') ) END) AS C_TaxCategory_IDR, " +
      "COALESCE(M_Product.IsGeneric, 'N') AS IsGeneric, " +
      "COALESCE(M_Product.IsPurchased, 'N') AS IsPurchased, " +
      "COALESCE(M_Product.IsSold, 'N') AS IsSold, " +
      "M_Product.Generic_Product_ID, " +
      "(CASE WHEN M_Product.Generic_Product_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL8.Name IS NULL THEN TO_CHAR(table8.Name) ELSE TO_CHAR(tableTRL8.Name) END)), ''))),'') ) END) AS Generic_Product_IDR, " +
      "M_Product.Description, " +
      "M_Product.ProductType, " +
      "(CASE WHEN M_Product.ProductType IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ProductTypeR, " +
      "COALESCE(M_Product.IsStocked, 'N') AS IsStocked, " +
      "M_Product.Weight, " +
      "M_Product.C_Uom_Weight_ID, " +
      "(CASE WHEN M_Product.C_Uom_Weight_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL10.Name IS NULL THEN TO_CHAR(table10.Name) ELSE TO_CHAR(tableTRL10.Name) END)), ''))),'') ) END) AS C_Uom_Weight_IDR, " +
      "M_Product.Costtype, " +
      "(CASE WHEN M_Product.Costtype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS CosttypeR, " +
      "M_Product.Coststd, " +
      "M_Product.M_AttributeSet_ID, " +
      "(CASE WHEN M_Product.M_AttributeSet_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table12.Name), ''))),'') ) END) AS M_AttributeSet_IDR, " +
      "M_Product.Attrsetvaluetype, " +
      "(CASE WHEN M_Product.Attrsetvaluetype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS AttrsetvaluetypeR, " +
      "M_Product.M_AttributeSetInstance_ID, " +
      "(CASE WHEN M_Product.M_AttributeSetInstance_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table13.Description), ''))),'') ) END) AS M_AttributeSetInstance_IDR, " +
      "COALESCE(M_Product.IsActive, 'N') AS IsActive, " +
      "M_Product.UPC, " +
      "M_Product.M_Brand_ID, " +
      "(CASE WHEN M_Product.M_Brand_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table14.Name), ''))),'') ) END) AS M_Brand_IDR, " +
      "M_Product.SalesRep_ID, " +
      "M_Product.C_BPartner_ID, " +
      "M_Product.ImageURL, " +
      "M_Product.DescriptionURL, " +
      "COALESCE(M_Product.Production, 'N') AS Production, " +
      "M_Product.MA_Processplan_ID, " +
      "(CASE WHEN M_Product.MA_Processplan_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table15.Name), ''))),'') ) END) AS MA_Processplan_IDR, " +
      "COALESCE(M_Product.IsSummary, 'N') AS IsSummary, " +
      "M_Product.M_Locator_ID, " +
      "M_Product.Volume, " +
      "M_Product.ShelfWidth, " +
      "M_Product.ShelfHeight, " +
      "M_Product.ShelfDepth, " +
      "M_Product.UnitsPerPallet, " +
      "COALESCE(M_Product.Discontinued, 'N') AS Discontinued, " +
      "M_Product.DiscontinuedBy, " +
      "COALESCE(M_Product.IsBOM, 'N') AS IsBOM, " +
      "COALESCE(M_Product.IsInvoicePrintDetails, 'N') AS IsInvoicePrintDetails, " +
      "COALESCE(M_Product.IsPickListPrintDetails, 'N') AS IsPickListPrintDetails, " +
      "COALESCE(M_Product.IsVerified, 'N') AS IsVerified, " +
      "M_Product.Processing, " +
      "M_Product.S_ExpenseType_ID, " +
      "COALESCE(M_Product.Isquantityvariable, 'N') AS Isquantityvariable, " +
      "M_Product.S_Resource_ID, " +
      "COALESCE(M_Product.Isdeferredrevenue, 'N') AS Isdeferredrevenue, " +
      "COALESCE(M_Product.Isdeferredexpense, 'N') AS Isdeferredexpense, " +
      "COALESCE(M_Product.Bookusingpoprice, 'N') AS Bookusingpoprice, " +
      "M_Product.Characteristic_Desc, " +
      "M_Product.Revplantype, " +
      "(CASE WHEN M_Product.Revplantype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list4.name),'') ) END) AS RevplantypeR, " +
      "M_Product.Periodnumber, " +
      "M_Product.DefaultPeriod, " +
      "(CASE WHEN M_Product.DefaultPeriod IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list5.name),'') ) END) AS DefaultPeriodR, " +
      "M_Product.Expplantype, " +
      "(CASE WHEN M_Product.Expplantype IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list6.name),'') ) END) AS ExpplantypeR, " +
      "M_Product.Periodnumber_Exp, " +
      "M_Product.DefaultPeriod_Exp, " +
      "(CASE WHEN M_Product.DefaultPeriod_Exp IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list7.name),'') ) END) AS DefaultPeriod_ExpR, " +
      "COALESCE(M_Product.Calculated, 'N') AS Calculated, " +
      "M_Product.Capacity, " +
      "M_Product.Delaymin, " +
      "M_Product.MRP_Planner_ID, " +
      "M_Product.MRP_Planningmethod_ID, " +
      "M_Product.Qtymax, " +
      "M_Product.Qtymin, " +
      "M_Product.Qtystd, " +
      "COALESCE(M_Product.Qtytype, 'N') AS Qtytype, " +
      "M_Product.Stockmin, " +
      "M_Product.CreateVariants, " +
      "M_Product.Updateinvariants, " +
      "COALESCE(M_Product.Islinkedtoproduct, 'N') AS Islinkedtoproduct, " +
      "M_Product.Prod_Cat_Selection, " +
      "(CASE WHEN M_Product.Prod_Cat_Selection IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list8.name),'') ) END) AS Prod_Cat_SelectionR, " +
      "M_Product.Product_Selection, " +
      "(CASE WHEN M_Product.Product_Selection IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list9.name),'') ) END) AS Product_SelectionR, " +
      "COALESCE(M_Product.Returnable, 'N') AS Returnable, " +
      "M_Product.Overdue_Return_Days, " +
      "COALESCE(M_Product.Ispricerulebased, 'N') AS Ispricerulebased, " +
      "M_Product.Quantity_Rule, " +
      "(CASE WHEN M_Product.Quantity_Rule IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list10.name),'') ) END) AS Quantity_RuleR, " +
      "COALESCE(M_Product.Unique_Per_Document, 'N') AS Unique_Per_Document, " +
      "COALESCE(M_Product.Print_Description, 'N') AS Print_Description, " +
      "M_Product.Relateprodcattoservice, " +
      "M_Product.Relateprodtoservice, " +
      "COALESCE(M_Product.Allow_Deferred_Sell, 'N') AS Allow_Deferred_Sell, " +
      "M_Product.Deferred_Sell_Max_Days, " +
      "M_Product.VersionNo, " +
      "M_Product.GuaranteeDays, " +
      "M_Product.M_FreightCategory_ID, " +
      "COALESCE(M_Product.Enforce_Attribute, 'N') AS Enforce_Attribute, " +
      "M_Product.AD_Client_ID, " +
      "M_Product.Help, " +
      "M_Product.Classification, " +
      "M_Product.Stock_Min, " +
      "M_Product.Name2, " +
      "M_Product.DownloadURL, " +
      "COALESCE(M_Product.Ispriceprinted, 'N') AS Ispriceprinted, " +
      "M_Product.DocumentNote, " +
      "M_Product.M_Product_ID, " +
      "M_Product.SKU, " +
      "        '' AS TR_BGCOLOR, '' as total_count," +
      "        ? AS LANGUAGE, '' AS AD_USER_CLIENT, '' AS AD_ORG_CLIENT" +
      "        FROM M_Product left join (select AD_Org_ID, Name from AD_Org) table1 on (M_Product.AD_Org_ID = table1.AD_Org_ID) left join (select C_UOM_ID, Name from C_UOM) table2 on (M_Product.C_UOM_ID = table2.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL2 on (table2.C_UOM_ID = tableTRL2.C_UOM_ID and tableTRL2.AD_Language = ?)  left join (select M_Product_Category_ID, Name from M_Product_Category) table4 on (M_Product.M_Product_Category_ID = table4.M_Product_Category_ID) left join (select M_Product_Category_ID,AD_Language, Name from M_Product_Category_TRL) tableTRL4 on (table4.M_Product_Category_ID = tableTRL4.M_Product_Category_ID and tableTRL4.AD_Language = ?)  left join (select C_TaxCategory_ID, Name from C_TaxCategory) table6 on (M_Product.C_TaxCategory_ID = table6.C_TaxCategory_ID) left join (select C_TaxCategory_ID,AD_Language, Name from C_TaxCategory_TRL) tableTRL6 on (table6.C_TaxCategory_ID = tableTRL6.C_TaxCategory_ID and tableTRL6.AD_Language = ?)  left join (select M_Product_ID, Name from M_Product) table8 on (M_Product.Generic_Product_ID =  table8.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL8 on (table8.M_Product_ID = tableTRL8.M_Product_ID and tableTRL8.AD_Language = ?)  left join ad_ref_list_v list1 on (M_Product.ProductType = list1.value and list1.ad_reference_id = '270' and list1.ad_language = ?)  left join (select C_UOM_ID, Name from C_UOM) table10 on (M_Product.C_Uom_Weight_ID =  table10.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL10 on (table10.C_UOM_ID = tableTRL10.C_UOM_ID and tableTRL10.AD_Language = ?)  left join ad_ref_list_v list2 on (M_Product.Costtype = list2.value and list2.ad_reference_id = '800025' and list2.ad_language = ?)  left join (select M_AttributeSet_ID, Name from M_AttributeSet) table12 on (M_Product.M_AttributeSet_ID = table12.M_AttributeSet_ID) left join ad_ref_list_v list3 on (M_Product.Attrsetvaluetype = list3.value and list3.ad_reference_id = '5AD08D5DF85549E0BCC0DEBDE4C0D340' and list3.ad_language = ?)  left join (select M_AttributeSetInstance_ID, Description from M_AttributeSetInstance) table13 on (M_Product.M_AttributeSetInstance_ID = table13.M_AttributeSetInstance_ID) left join (select M_Brand_ID, Name from M_Brand) table14 on (M_Product.M_Brand_ID = table14.M_Brand_ID) left join (select MA_Processplan_ID, Name from MA_Processplan) table15 on (M_Product.MA_Processplan_ID = table15.MA_Processplan_ID) left join ad_ref_list_v list4 on (M_Product.Revplantype = list4.value and list4.ad_reference_id = '73625A8F22EF4CD7808603156BA606D7' and list4.ad_language = ?)  left join ad_ref_list_v list5 on (M_Product.DefaultPeriod = list5.value and list5.ad_reference_id = '6669508E338F4A10BA3E0D241D133E62' and list5.ad_language = ?)  left join ad_ref_list_v list6 on (M_Product.Expplantype = list6.value and list6.ad_reference_id = '73625A8F22EF4CD7808603156BA606D7' and list6.ad_language = ?)  left join ad_ref_list_v list7 on (M_Product.DefaultPeriod_Exp = list7.value and list7.ad_reference_id = '6669508E338F4A10BA3E0D241D133E62' and list7.ad_language = ?)  left join ad_ref_list_v list8 on (M_Product.Prod_Cat_Selection = list8.value and list8.ad_reference_id = '800029' and list8.ad_language = ?)  left join ad_ref_list_v list9 on (M_Product.Product_Selection = list9.value and list9.ad_reference_id = '800029' and list9.ad_language = ?)  left join ad_ref_list_v list10 on (M_Product.Quantity_Rule = list10.value and list10.ad_reference_id = '4E07601C34764669B75FCA1808F55B57' and list10.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((parValue==null || parValue.equals(""))?"":"  AND (M_Product.Value) LIKE (?) ");
    strSql = strSql + ((parName==null || parName.equals(""))?"":"  AND C_IGNORE_ACCENT(M_Product.Name) LIKE C_IGNORE_ACCENT(?) ");
    strSql = strSql + ((parM_Product_Category_ID==null || parM_Product_Category_ID.equals(""))?"":"  AND (M_Product.M_Product_Category_ID) = (?) ");
    strSql = strSql + ((parProductType==null || parProductType.equals(""))?"":"  AND (M_Product.ProductType) = (?) ");
    strSql = strSql + ((parC_TaxCategory_ID==null || parC_TaxCategory_ID.equals(""))?"":"  AND (M_Product.C_TaxCategory_ID) = (?) ");
    strSql = strSql + ((parSessionDate.equals("parSessionDate"))?"  AND 1 = 1 ":"");
    strSql = strSql + ((parSessionUser.equals("parSessionUser"))?"  AND 1 = 1 ":"");
    strSql = strSql + 
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        ORDER BY ";
    strSql = strSql + ((orderbyclause==null || orderbyclause.equals(""))?"":orderbyclause);
    strSql = strSql + 
      ", 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (parValue != null && !(parValue.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parValue);
      }
      if (parName != null && !(parName.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parName);
      }
      if (parM_Product_Category_ID != null && !(parM_Product_Category_ID.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parM_Product_Category_ID);
      }
      if (parProductType != null && !(parProductType.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parProductType);
      }
      if (parC_TaxCategory_ID != null && !(parC_TaxCategory_ID.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, parC_TaxCategory_ID);
      }
      if (parSessionDate != null && !(parSessionDate.equals(""))) {
        }
      if (parSessionUser != null && !(parSessionUser.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (orderbyclause != null && !(orderbyclause.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Product228117210CAE44B9868C8CA24B810FD6Data objectProduct228117210CAE44B9868C8CA24B810FD6Data = new Product228117210CAE44B9868C8CA24B810FD6Data();
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.created = UtilSql.getValue(result, "created");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updated = UtilSql.getValue(result, "updated");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.managevariants = UtilSql.getValue(result, "managevariants");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.value = UtilSql.getValue(result, "value");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.name = UtilSql.getValue(result, "name");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adImageId = UtilSql.getValue(result, "ad_image_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mProductCategoryId = UtilSql.getValue(result, "m_product_category_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mProductCategoryIdr = UtilSql.getValue(result, "m_product_category_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cTaxcategoryId = UtilSql.getValue(result, "c_taxcategory_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cTaxcategoryIdr = UtilSql.getValue(result, "c_taxcategory_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isgeneric = UtilSql.getValue(result, "isgeneric");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispurchased = UtilSql.getValue(result, "ispurchased");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.issold = UtilSql.getValue(result, "issold");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.genericProductId = UtilSql.getValue(result, "generic_product_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.genericProductIdr = UtilSql.getValue(result, "generic_product_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.description = UtilSql.getValue(result, "description");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.producttype = UtilSql.getValue(result, "producttype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.producttyper = UtilSql.getValue(result, "producttyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isstocked = UtilSql.getValue(result, "isstocked");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.weight = UtilSql.getValue(result, "weight");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomWeightId = UtilSql.getValue(result, "c_uom_weight_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cUomWeightIdr = UtilSql.getValue(result, "c_uom_weight_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.costtype = UtilSql.getValue(result, "costtype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.costtyper = UtilSql.getValue(result, "costtyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.coststd = UtilSql.getValue(result, "coststd");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetId = UtilSql.getValue(result, "m_attributeset_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetIdr = UtilSql.getValue(result, "m_attributeset_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.attrsetvaluetype = UtilSql.getValue(result, "attrsetvaluetype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.attrsetvaluetyper = UtilSql.getValue(result, "attrsetvaluetyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mAttributesetinstanceIdr = UtilSql.getValue(result, "m_attributesetinstance_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isactive = UtilSql.getValue(result, "isactive");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.upc = UtilSql.getValue(result, "upc");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mBrandId = UtilSql.getValue(result, "m_brand_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mBrandIdr = UtilSql.getValue(result, "m_brand_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.imageurl = UtilSql.getValue(result, "imageurl");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.descriptionurl = UtilSql.getValue(result, "descriptionurl");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.production = UtilSql.getValue(result, "production");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.maProcessplanId = UtilSql.getValue(result, "ma_processplan_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.maProcessplanIdr = UtilSql.getValue(result, "ma_processplan_idr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.issummary = UtilSql.getValue(result, "issummary");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mLocatorId = UtilSql.getValue(result, "m_locator_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.volume = UtilSql.getValue(result, "volume");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.shelfwidth = UtilSql.getValue(result, "shelfwidth");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.shelfheight = UtilSql.getValue(result, "shelfheight");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.shelfdepth = UtilSql.getValue(result, "shelfdepth");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.unitsperpallet = UtilSql.getValue(result, "unitsperpallet");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.discontinued = UtilSql.getValue(result, "discontinued");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.discontinuedby = UtilSql.getDateValue(result, "discontinuedby", "dd-MM-yyyy");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isbom = UtilSql.getValue(result, "isbom");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isinvoiceprintdetails = UtilSql.getValue(result, "isinvoiceprintdetails");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispicklistprintdetails = UtilSql.getValue(result, "ispicklistprintdetails");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isverified = UtilSql.getValue(result, "isverified");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.processing = UtilSql.getValue(result, "processing");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.sExpensetypeId = UtilSql.getValue(result, "s_expensetype_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isquantityvariable = UtilSql.getValue(result, "isquantityvariable");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.sResourceId = UtilSql.getValue(result, "s_resource_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isdeferredrevenue = UtilSql.getValue(result, "isdeferredrevenue");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.isdeferredexpense = UtilSql.getValue(result, "isdeferredexpense");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.bookusingpoprice = UtilSql.getValue(result, "bookusingpoprice");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.characteristicDesc = UtilSql.getValue(result, "characteristic_desc");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.revplantype = UtilSql.getValue(result, "revplantype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.revplantyper = UtilSql.getValue(result, "revplantyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.periodnumber = UtilSql.getValue(result, "periodnumber");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiod = UtilSql.getValue(result, "defaultperiod");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiodr = UtilSql.getValue(result, "defaultperiodr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.expplantype = UtilSql.getValue(result, "expplantype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.expplantyper = UtilSql.getValue(result, "expplantyper");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.periodnumberExp = UtilSql.getValue(result, "periodnumber_exp");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiodExp = UtilSql.getValue(result, "defaultperiod_exp");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.defaultperiodExpr = UtilSql.getValue(result, "defaultperiod_expr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.calculated = UtilSql.getValue(result, "calculated");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.capacity = UtilSql.getValue(result, "capacity");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.delaymin = UtilSql.getValue(result, "delaymin");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mrpPlannerId = UtilSql.getValue(result, "mrp_planner_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mrpPlanningmethodId = UtilSql.getValue(result, "mrp_planningmethod_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtymax = UtilSql.getValue(result, "qtymax");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtymin = UtilSql.getValue(result, "qtymin");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtystd = UtilSql.getValue(result, "qtystd");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.qtytype = UtilSql.getValue(result, "qtytype");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.stockmin = UtilSql.getValue(result, "stockmin");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.createvariants = UtilSql.getValue(result, "createvariants");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.updateinvariants = UtilSql.getValue(result, "updateinvariants");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.islinkedtoproduct = UtilSql.getValue(result, "islinkedtoproduct");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.prodCatSelection = UtilSql.getValue(result, "prod_cat_selection");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.prodCatSelectionr = UtilSql.getValue(result, "prod_cat_selectionr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.productSelection = UtilSql.getValue(result, "product_selection");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.productSelectionr = UtilSql.getValue(result, "product_selectionr");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.returnable = UtilSql.getValue(result, "returnable");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispricerulebased = UtilSql.getValue(result, "ispricerulebased");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.quantityRule = UtilSql.getValue(result, "quantity_rule");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.quantityRuler = UtilSql.getValue(result, "quantity_ruler");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.uniquePerDocument = UtilSql.getValue(result, "unique_per_document");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.printDescription = UtilSql.getValue(result, "print_description");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.relateprodcattoservice = UtilSql.getValue(result, "relateprodcattoservice");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.relateprodtoservice = UtilSql.getValue(result, "relateprodtoservice");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.allowDeferredSell = UtilSql.getValue(result, "allow_deferred_sell");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.deferredSellMaxDays = UtilSql.getValue(result, "deferred_sell_max_days");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.versionno = UtilSql.getValue(result, "versionno");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.guaranteedays = UtilSql.getValue(result, "guaranteedays");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mFreightcategoryId = UtilSql.getValue(result, "m_freightcategory_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.enforceAttribute = UtilSql.getValue(result, "enforce_attribute");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.help = UtilSql.getValue(result, "help");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.classification = UtilSql.getValue(result, "classification");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.stockMin = UtilSql.getValue(result, "stock_min");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.name2 = UtilSql.getValue(result, "name2");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.downloadurl = UtilSql.getValue(result, "downloadurl");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.ispriceprinted = UtilSql.getValue(result, "ispriceprinted");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.documentnote = UtilSql.getValue(result, "documentnote");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.mProductId = UtilSql.getValue(result, "m_product_id");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.sku = UtilSql.getValue(result, "sku");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.trBgcolor = UtilSql.getValue(result, "tr_bgcolor");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.totalCount = UtilSql.getValue(result, "total_count");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.language = UtilSql.getValue(result, "language");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adUserClient = UtilSql.getValue(result, "ad_user_client");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.adOrgClient = UtilSql.getValue(result, "ad_org_client");
        objectProduct228117210CAE44B9868C8CA24B810FD6Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProduct228117210CAE44B9868C8CA24B810FD6Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Product228117210CAE44B9868C8CA24B810FD6Data objectProduct228117210CAE44B9868C8CA24B810FD6Data[] = new Product228117210CAE44B9868C8CA24B810FD6Data[vector.size()];
    vector.copyInto(objectProduct228117210CAE44B9868C8CA24B810FD6Data);
    return(objectProduct228117210CAE44B9868C8CA24B810FD6Data);
  }

/**
Create a registry
 */
  public static Product228117210CAE44B9868C8CA24B810FD6Data[] set(String mProductId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String name, String description, String issummary, String cUomId, String isstocked, String ispurchased, String issold, String volume, String weight, String revplantype, String quantityRule, String value, String mProductCategoryId, String cTaxcategoryId, String upc, String sku, String shelfwidth, String shelfheight, String shelfdepth, String unitsperpallet, String discontinued, String discontinuedby, String defaultperiod, String allowDeferredSell, String relateprodtoservice, String documentnote, String help, String classification, String salesrepId, String bookusingpoprice, String isbom, String isinvoiceprintdetails, String ispicklistprintdetails, String isverified, String processing, String islinkedtoproduct, String capacity, String delaymin, String mrpPlannerId, String mrpPlanningmethodId, String qtymax, String qtymin, String qtystd, String qtytype, String stockmin, String productSelection, String uniquePerDocument, String overdueReturnDays, String relateprodcattoservice, String sExpensetypeId, String sResourceId, String ispricerulebased, String producttype, String imageurl, String descriptionurl, String versionno, String guaranteedays, String deferredSellMaxDays, String attrsetvaluetype, String adImageId, String cBpartnerId, String ispriceprinted, String name2, String costtype, String coststd, String stockMin, String enforceAttribute, String calculated, String maProcessplanId, String production, String isdeferredrevenue, String mAttributesetId, String mAttributesetinstanceId, String mAttributesetinstanceIdr, String downloadurl, String mFreightcategoryId, String mLocatorId, String isdeferredexpense, String printDescription, String prodCatSelection, String defaultperiodExp, String expplantype, String periodnumberExp, String updateinvariants, String returnable, String mBrandId, String isgeneric, String genericProductId, String createvariants, String characteristicDesc, String managevariants, String cUomWeightId, String isquantityvariable, String periodnumber)    throws ServletException {
    Product228117210CAE44B9868C8CA24B810FD6Data objectProduct228117210CAE44B9868C8CA24B810FD6Data[] = new Product228117210CAE44B9868C8CA24B810FD6Data[1];
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0] = new Product228117210CAE44B9868C8CA24B810FD6Data();
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].created = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].createdbyr = createdbyr;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].updated = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].updatedTimeStamp = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].updatedby = updatedby;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].updatedbyr = updatedbyr;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].adOrgId = adOrgId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].adOrgIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].managevariants = managevariants;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].value = value;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].name = name;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].adImageId = adImageId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cUomId = cUomId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cUomIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mProductCategoryId = mProductCategoryId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mProductCategoryIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cTaxcategoryId = cTaxcategoryId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cTaxcategoryIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isgeneric = isgeneric;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].ispurchased = ispurchased;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].issold = issold;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].genericProductId = genericProductId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].genericProductIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].description = description;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].producttype = producttype;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].producttyper = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isstocked = isstocked;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].weight = weight;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cUomWeightId = cUomWeightId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cUomWeightIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].costtype = costtype;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].costtyper = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].coststd = coststd;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mAttributesetId = mAttributesetId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mAttributesetIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].attrsetvaluetype = attrsetvaluetype;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].attrsetvaluetyper = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mAttributesetinstanceId = mAttributesetinstanceId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mAttributesetinstanceIdr = mAttributesetinstanceIdr;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isactive = isactive;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].upc = upc;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mBrandId = mBrandId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mBrandIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].salesrepId = salesrepId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].cBpartnerId = cBpartnerId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].imageurl = imageurl;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].descriptionurl = descriptionurl;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].production = production;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].maProcessplanId = maProcessplanId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].maProcessplanIdr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].issummary = issummary;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mLocatorId = mLocatorId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].volume = volume;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].shelfwidth = shelfwidth;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].shelfheight = shelfheight;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].shelfdepth = shelfdepth;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].unitsperpallet = unitsperpallet;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].discontinued = discontinued;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].discontinuedby = discontinuedby;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isbom = isbom;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isinvoiceprintdetails = isinvoiceprintdetails;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].ispicklistprintdetails = ispicklistprintdetails;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isverified = isverified;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].processing = processing;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].sExpensetypeId = sExpensetypeId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isquantityvariable = isquantityvariable;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].sResourceId = sResourceId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isdeferredrevenue = isdeferredrevenue;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].isdeferredexpense = isdeferredexpense;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].bookusingpoprice = bookusingpoprice;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].characteristicDesc = characteristicDesc;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].revplantype = revplantype;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].revplantyper = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].periodnumber = periodnumber;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].defaultperiod = defaultperiod;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].defaultperiodr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].expplantype = expplantype;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].expplantyper = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].periodnumberExp = periodnumberExp;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].defaultperiodExp = defaultperiodExp;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].defaultperiodExpr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].calculated = calculated;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].capacity = capacity;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].delaymin = delaymin;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mrpPlannerId = mrpPlannerId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mrpPlanningmethodId = mrpPlanningmethodId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].qtymax = qtymax;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].qtymin = qtymin;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].qtystd = qtystd;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].qtytype = qtytype;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].stockmin = stockmin;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].createvariants = createvariants;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].updateinvariants = updateinvariants;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].islinkedtoproduct = islinkedtoproduct;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].prodCatSelection = prodCatSelection;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].prodCatSelectionr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].productSelection = productSelection;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].productSelectionr = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].returnable = returnable;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].overdueReturnDays = overdueReturnDays;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].ispricerulebased = ispricerulebased;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].quantityRule = quantityRule;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].quantityRuler = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].uniquePerDocument = uniquePerDocument;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].printDescription = printDescription;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].relateprodcattoservice = relateprodcattoservice;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].relateprodtoservice = relateprodtoservice;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].allowDeferredSell = allowDeferredSell;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].deferredSellMaxDays = deferredSellMaxDays;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].versionno = versionno;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].guaranteedays = guaranteedays;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mFreightcategoryId = mFreightcategoryId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].enforceAttribute = enforceAttribute;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].adClientId = adClientId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].help = help;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].classification = classification;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].stockMin = stockMin;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].name2 = name2;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].downloadurl = downloadurl;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].ispriceprinted = ispriceprinted;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].documentnote = documentnote;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].mProductId = mProductId;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].sku = sku;
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].trBgcolor = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].totalCount = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].language = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].adUserClient = "";
    objectProduct228117210CAE44B9868C8CA24B810FD6Data[0].adOrgClient = "";
    return objectProduct228117210CAE44B9868C8CA24B810FD6Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef1407_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef1409_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef8418_2(ConnectionProvider connectionProvider, String M_AttributeSetInstance_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Description), ''))), '') ) as M_AttributeSetInstance_ID FROM M_AttributeSetInstance left join (select M_AttributeSetInstance_ID, Description from M_AttributeSetInstance) table2 on (M_AttributeSetInstance.M_AttributeSetInstance_ID = table2.M_AttributeSetInstance_ID) WHERE M_AttributeSetInstance.isActive='Y' AND M_AttributeSetInstance.M_AttributeSetInstance_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_AttributeSetInstance_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_attributesetinstance_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE M_Product" +
      "        SET AD_Org_ID = (?) , ManageVariants = (?) , Value = (?) , Name = (?) , AD_Image_ID = (?) , C_UOM_ID = (?) , M_Product_Category_ID = (?) , C_TaxCategory_ID = (?) , IsGeneric = (?) , IsPurchased = (?) , IsSold = (?) , Generic_Product_ID = (?) , Description = (?) , ProductType = (?) , IsStocked = (?) , Weight = TO_NUMBER(?) , C_Uom_Weight_ID = (?) , Costtype = (?) , Coststd = TO_NUMBER(?) , M_AttributeSet_ID = (?) , Attrsetvaluetype = (?) , M_AttributeSetInstance_ID = (?) , IsActive = (?) , UPC = (?) , M_Brand_ID = (?) , SalesRep_ID = (?) , C_BPartner_ID = (?) , ImageURL = (?) , DescriptionURL = (?) , Production = (?) , MA_Processplan_ID = (?) , IsSummary = (?) , M_Locator_ID = (?) , Volume = TO_NUMBER(?) , ShelfWidth = TO_NUMBER(?) , ShelfHeight = TO_NUMBER(?) , ShelfDepth = TO_NUMBER(?) , UnitsPerPallet = TO_NUMBER(?) , Discontinued = (?) , DiscontinuedBy = TO_DATE(?) , IsBOM = (?) , IsInvoicePrintDetails = (?) , IsPickListPrintDetails = (?) , IsVerified = (?) , Processing = (?) , S_ExpenseType_ID = (?) , Isquantityvariable = (?) , S_Resource_ID = (?) , Isdeferredrevenue = (?) , Isdeferredexpense = (?) , Bookusingpoprice = (?) , Characteristic_Desc = (?) , Revplantype = (?) , Periodnumber = TO_NUMBER(?) , DefaultPeriod = (?) , Expplantype = (?) , Periodnumber_Exp = TO_NUMBER(?) , DefaultPeriod_Exp = (?) , Calculated = (?) , Capacity = TO_NUMBER(?) , Delaymin = TO_NUMBER(?) , MRP_Planner_ID = (?) , MRP_Planningmethod_ID = (?) , Qtymax = TO_NUMBER(?) , Qtymin = TO_NUMBER(?) , Qtystd = TO_NUMBER(?) , Qtytype = (?) , Stockmin = TO_NUMBER(?) , CreateVariants = (?) , Updateinvariants = (?) , Islinkedtoproduct = (?) , Prod_Cat_Selection = (?) , Product_Selection = (?) , Returnable = (?) , Overdue_Return_Days = TO_NUMBER(?) , Ispricerulebased = (?) , Quantity_Rule = (?) , Unique_Per_Document = (?) , Print_Description = (?) , Relateprodcattoservice = (?) , Relateprodtoservice = (?) , Allow_Deferred_Sell = (?) , Deferred_Sell_Max_Days = TO_NUMBER(?) , VersionNo = (?) , GuaranteeDays = TO_NUMBER(?) , M_FreightCategory_ID = (?) , Enforce_Attribute = (?) , AD_Client_ID = (?) , Help = (?) , Classification = (?) , Stock_Min = TO_NUMBER(?) , Name2 = (?) , DownloadURL = (?) , Ispriceprinted = (?) , DocumentNote = (?) , M_Product_ID = (?) , SKU = (?) , updated = now(), updatedby = ? " +
      "        WHERE M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managevariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImageId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isgeneric);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispurchased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issold);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, genericProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, producttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isstocked);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, weight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomWeightId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coststd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, attrsetvaluetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, upc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mBrandId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, imageurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descriptionurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, production);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maProcessplanId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issummary);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, volume);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfwidth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfheight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfdepth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, unitsperpallet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinued);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinuedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isbom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiceprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispicklistprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isverified);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sExpensetypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isquantityvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredrevenue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredexpense);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bookusingpoprice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, characteristicDesc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, revplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, expplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumberExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiodExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculated);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, capacity);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, delaymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlannerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlanningmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymax);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtystd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtytype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updateinvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, islinkedtoproduct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prodCatSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, productSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, returnable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispricerulebased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityRule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, uniquePerDocument);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodcattoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodtoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allowDeferredSell);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deferredSellMaxDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, versionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, guaranteedays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mFreightcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, enforceAttribute);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, help);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, classification);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockMin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, downloadurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispriceprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentnote);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sku);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO M_Product " +
      "        (AD_Org_ID, ManageVariants, Value, Name, AD_Image_ID, C_UOM_ID, M_Product_Category_ID, C_TaxCategory_ID, IsGeneric, IsPurchased, IsSold, Generic_Product_ID, Description, ProductType, IsStocked, Weight, C_Uom_Weight_ID, Costtype, Coststd, M_AttributeSet_ID, Attrsetvaluetype, M_AttributeSetInstance_ID, IsActive, UPC, M_Brand_ID, SalesRep_ID, C_BPartner_ID, ImageURL, DescriptionURL, Production, MA_Processplan_ID, IsSummary, M_Locator_ID, Volume, ShelfWidth, ShelfHeight, ShelfDepth, UnitsPerPallet, Discontinued, DiscontinuedBy, IsBOM, IsInvoicePrintDetails, IsPickListPrintDetails, IsVerified, Processing, S_ExpenseType_ID, Isquantityvariable, S_Resource_ID, Isdeferredrevenue, Isdeferredexpense, Bookusingpoprice, Characteristic_Desc, Revplantype, Periodnumber, DefaultPeriod, Expplantype, Periodnumber_Exp, DefaultPeriod_Exp, Calculated, Capacity, Delaymin, MRP_Planner_ID, MRP_Planningmethod_ID, Qtymax, Qtymin, Qtystd, Qtytype, Stockmin, CreateVariants, Updateinvariants, Islinkedtoproduct, Prod_Cat_Selection, Product_Selection, Returnable, Overdue_Return_Days, Ispricerulebased, Quantity_Rule, Unique_Per_Document, Print_Description, Relateprodcattoservice, Relateprodtoservice, Allow_Deferred_Sell, Deferred_Sell_Max_Days, VersionNo, GuaranteeDays, M_FreightCategory_ID, Enforce_Attribute, AD_Client_ID, Help, Classification, Stock_Min, Name2, DownloadURL, Ispriceprinted, DocumentNote, M_Product_ID, SKU, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managevariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, value);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adImageId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isgeneric);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispurchased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issold);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, genericProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, producttype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isstocked);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, weight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomWeightId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, costtype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coststd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, attrsetvaluetype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, upc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mBrandId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, imageurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descriptionurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, production);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maProcessplanId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issummary);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mLocatorId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, volume);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfwidth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfheight);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, shelfdepth);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, unitsperpallet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinued);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discontinuedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isbom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiceprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispicklistprintdetails);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isverified);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sExpensetypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isquantityvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredrevenue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdeferredexpense);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bookusingpoprice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, characteristicDesc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, revplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumber);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiod);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, expplantype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, periodnumberExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, defaultperiodExp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculated);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, capacity);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, delaymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlannerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mrpPlanningmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymax);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtymin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtystd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtytype);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockmin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updateinvariants);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, islinkedtoproduct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, prodCatSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, productSelection);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, returnable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispricerulebased);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityRule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, uniquePerDocument);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodcattoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateprodtoservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, allowDeferredSell);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deferredSellMaxDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, versionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, guaranteedays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mFreightcategoryId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, enforceAttribute);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, help);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, classification);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, stockMin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, downloadurl);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispriceprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentnote);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sku);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM M_Product" +
      "        WHERE M_Product.M_Product_ID = ? " +
      "        AND M_Product.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Product.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM M_Product" +
      "         WHERE M_Product.M_Product_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM M_Product" +
      "         WHERE M_Product.M_Product_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
