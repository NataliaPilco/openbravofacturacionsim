//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.Vacaciones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Linea030D19C3A2434EC5A02B5113AF75FB84Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Linea030D19C3A2434EC5A02B5113AF75FB84Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String procesar;
  public String noVacacionId;
  public String line;
  public String fechaInicio;
  public String fechaFin;
  public String estado;
  public String estador;
  public String isactive;
  public String dias;
  public String procesado;
  public String adClientId;
  public String noVacacionLineaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("no_vacacion_id") || fieldName.equals("noVacacionId"))
      return noVacacionId;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("estador"))
      return estador;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("dias"))
      return dias;
    else if (fieldName.equalsIgnoreCase("procesado"))
      return procesado;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_vacacion_linea_id") || fieldName.equals("noVacacionLineaId"))
      return noVacacionLineaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Linea030D19C3A2434EC5A02B5113AF75FB84Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noVacacionId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, noVacacionId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Linea030D19C3A2434EC5A02B5113AF75FB84Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noVacacionId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_vacacion_linea.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_vacacion_linea.CreatedBy) as CreatedByR, " +
      "        to_char(no_vacacion_linea.Updated, ?) as updated, " +
      "        to_char(no_vacacion_linea.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_vacacion_linea.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_vacacion_linea.UpdatedBy) as UpdatedByR," +
      "        no_vacacion_linea.AD_Org_ID, " +
      "no_vacacion_linea.Procesar, " +
      "no_vacacion_linea.NO_Vacacion_ID, " +
      "no_vacacion_linea.Line, " +
      "no_vacacion_linea.Fecha_Inicio, " +
      "no_vacacion_linea.Fecha_Fin, " +
      "no_vacacion_linea.Estado, " +
      "(CASE WHEN no_vacacion_linea.Estado IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS EstadoR, " +
      "COALESCE(no_vacacion_linea.Isactive, 'N') AS Isactive, " +
      "no_vacacion_linea.dias, " +
      "COALESCE(no_vacacion_linea.Procesado, 'N') AS Procesado, " +
      "no_vacacion_linea.AD_Client_ID, " +
      "no_vacacion_linea.NO_Vacacion_Linea_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_vacacion_linea left join ad_ref_list_v list1 on (no_vacacion_linea.Estado = list1.value and list1.ad_reference_id = '6EE3EA7321544309803D4992D165F316' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((noVacacionId==null || noVacacionId.equals(""))?"":"  AND no_vacacion_linea.NO_Vacacion_ID = ?  ");
    strSql = strSql + 
      "        AND no_vacacion_linea.NO_Vacacion_Linea_ID = ? " +
      "        AND no_vacacion_linea.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_vacacion_linea.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (noVacacionId != null && !(noVacacionId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Linea030D19C3A2434EC5A02B5113AF75FB84Data objectLinea030D19C3A2434EC5A02B5113AF75FB84Data = new Linea030D19C3A2434EC5A02B5113AF75FB84Data();
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.created = UtilSql.getValue(result, "created");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.updated = UtilSql.getValue(result, "updated");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.procesar = UtilSql.getValue(result, "procesar");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.noVacacionId = UtilSql.getValue(result, "no_vacacion_id");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.line = UtilSql.getValue(result, "line");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.estado = UtilSql.getValue(result, "estado");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.estador = UtilSql.getValue(result, "estador");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.isactive = UtilSql.getValue(result, "isactive");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.dias = UtilSql.getValue(result, "dias");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.procesado = UtilSql.getValue(result, "procesado");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.noVacacionLineaId = UtilSql.getValue(result, "no_vacacion_linea_id");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.language = UtilSql.getValue(result, "language");
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.adUserClient = "";
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.adOrgClient = "";
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.createdby = "";
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.trBgcolor = "";
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.totalCount = "";
        objectLinea030D19C3A2434EC5A02B5113AF75FB84Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLinea030D19C3A2434EC5A02B5113AF75FB84Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Linea030D19C3A2434EC5A02B5113AF75FB84Data objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[] = new Linea030D19C3A2434EC5A02B5113AF75FB84Data[vector.size()];
    vector.copyInto(objectLinea030D19C3A2434EC5A02B5113AF75FB84Data);
    return(objectLinea030D19C3A2434EC5A02B5113AF75FB84Data);
  }

/**
Create a registry
 */
  public static Linea030D19C3A2434EC5A02B5113AF75FB84Data[] set(String noVacacionId, String adOrgId, String updatedby, String updatedbyr, String isactive, String fechaInicio, String procesar, String estado, String procesado, String dias, String line, String createdby, String createdbyr, String fechaFin, String adClientId, String noVacacionLineaId)    throws ServletException {
    Linea030D19C3A2434EC5A02B5113AF75FB84Data objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[] = new Linea030D19C3A2434EC5A02B5113AF75FB84Data[1];
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0] = new Linea030D19C3A2434EC5A02B5113AF75FB84Data();
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].created = "";
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].createdbyr = createdbyr;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].updated = "";
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].updatedTimeStamp = "";
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].updatedby = updatedby;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].updatedbyr = updatedbyr;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].adOrgId = adOrgId;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].procesar = procesar;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].noVacacionId = noVacacionId;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].line = line;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].fechaInicio = fechaInicio;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].fechaFin = fechaFin;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].estado = estado;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].estador = "";
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].isactive = isactive;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].dias = dias;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].procesado = procesado;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].adClientId = adClientId;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].noVacacionLineaId = noVacacionLineaId;
    objectLinea030D19C3A2434EC5A02B5113AF75FB84Data[0].language = "";
    return objectLinea030D19C3A2434EC5A02B5113AF75FB84Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef09C643311F614718AEB45A28A675F87E_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef55746681A916405D852E21D517F300E3(ConnectionProvider connectionProvider, String NO_Vacacion_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(LINE),0)+10 AS DefaultValue FROM NO_VACACION_LINEA WHERE NO_VACACION_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, NO_Vacacion_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA546C38F45AF4DED94271CC975163646_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT no_vacacion_linea.NO_Vacacion_ID AS NAME" +
      "        FROM no_vacacion_linea" +
      "        WHERE no_vacacion_linea.NO_Vacacion_Linea_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String paramLanguage, String noVacacionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (list1.name || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))) AS NAME FROM no_vacacion left join (select NO_Vacacion_ID, Anio, C_Bpartner_ID from NO_Vacacion) table1 on (no_vacacion.NO_Vacacion_ID = table1.NO_Vacacion_ID) left join ad_ref_list_v list1 on (table1.Anio = list1.value and list1.ad_reference_id = 'DBE0CE97929A4EB3A2093E35875EA82B' and list1.ad_language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table2 on (table1.C_Bpartner_ID = table2.C_BPartner_ID) WHERE no_vacacion.NO_Vacacion_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String paramLanguage, String noVacacionId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (list1.name || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))) AS NAME FROM no_vacacion left join (select NO_Vacacion_ID, Anio, C_Bpartner_ID from NO_Vacacion) table1 on (no_vacacion.NO_Vacacion_ID = table1.NO_Vacacion_ID) left join ad_ref_list_v list1 on (table1.Anio = list1.value and list1.ad_reference_id = 'DBE0CE97929A4EB3A2093E35875EA82B' and list1.ad_language = ?)  left join (select C_BPartner_ID, Name from C_BPartner) table2 on (table1.C_Bpartner_ID = table2.C_BPartner_ID) WHERE no_vacacion.NO_Vacacion_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_vacacion_linea" +
      "        SET AD_Org_ID = (?) , Procesar = (?) , NO_Vacacion_ID = (?) , Line = TO_NUMBER(?) , Fecha_Inicio = TO_DATE(?) , Fecha_Fin = TO_DATE(?) , Estado = (?) , Isactive = (?) , dias = TO_NUMBER(?) , Procesado = (?) , AD_Client_ID = (?) , NO_Vacacion_Linea_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_vacacion_linea.NO_Vacacion_Linea_ID = ? " +
      "                 AND no_vacacion_linea.NO_Vacacion_ID = ? " +
      "        AND no_vacacion_linea.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_vacacion_linea.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionLineaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionLineaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_vacacion_linea " +
      "        (AD_Org_ID, Procesar, NO_Vacacion_ID, Line, Fecha_Inicio, Fecha_Fin, Estado, Isactive, dias, Procesado, AD_Client_ID, NO_Vacacion_Linea_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), TO_NUMBER(?), TO_DATE(?), TO_DATE(?), (?), (?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionLineaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String noVacacionId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_vacacion_linea" +
      "        WHERE no_vacacion_linea.NO_Vacacion_Linea_ID = ? " +
      "                 AND no_vacacion_linea.NO_Vacacion_ID = ? " +
      "        AND no_vacacion_linea.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_vacacion_linea.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_vacacion_linea" +
      "         WHERE no_vacacion_linea.NO_Vacacion_Linea_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_vacacion_linea" +
      "         WHERE no_vacacion_linea.NO_Vacacion_Linea_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
