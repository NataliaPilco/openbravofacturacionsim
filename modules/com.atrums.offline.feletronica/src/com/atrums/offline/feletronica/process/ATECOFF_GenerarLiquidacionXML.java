package com.atrums.offline.feletronica.process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;

/**
 * @author ATRUMS-IT
 *
 */
public class ATECOFF_GenerarLiquidacionXML {

	public static boolean generarFacturaXMLLiq(Invoice invDato, ConnectionProvider conn, String strUser,
			boolean enviarSRI, OBError msg) throws Exception {

		/**
		 * Creando componentes para generar XML
		 */
		File flXml = null;
		String fileString = null;
		Document docXML = null;
		Client cltDato = null;
		OutputFormat ofFormat = null;
		DocumentType dctDato = null;
		OrganizationInformation oriDato = null;
		BusinessPartner bspDato = null;
		ATECOFFGenerarXmlData[] axmlDirMatriz = null;
		ATECOFFGenerarXmlData[] axmlDirec = null;
		ATECOFFGenerarXmlData[] axmlImpuestos = null;
		ATECOFFGenerarXmlData[] axmlDetalles = null;
		ATECOFFGenerarXmlData[] axmldetImps = null;
		ATECOFFGenerarXmlData[] axmlMinout = null;
		ATECOFFGenerarXmlData[] axmlEmail = null;

		/* NPI */
		ATECOFFGenerarXmlData[] axmlContribuyente = null;
		ATECOFFGenerarXmlData[] axmlDirProv = null;
		ATECOFFGenerarXmlData[] axmlTotImpRee = null;
		ATECOFFGenerarXmlData[] axmlDescuentoFac = null;
		ATECOFFGenerarXmlData[] axmlReembolsos = null;
		ATECOFFGenerarXmlData[] axmlReembolsosDet = null;
		ATECOFFGenerarXmlData[] axmlImpReem = null;
		ATECOFFGenerarXmlData[] axmlVerificarReem = null;
		ATECOFFGenerarXmlData[] axmlMaqFiscal = null;
		String type = null;

		String emailSoporte = "soporte@atrums.com";

		Hashtable<String, String> hstClaveAcceso = new Hashtable<String, String>();

		try {
			flXml = File.createTempFile("documento", ".xml", null);
			flXml.deleteOnExit();
			docXML = DocumentHelper.createDocument();
			ofFormat = OutputFormat.createPrettyPrint();

			dctDato = OBDal.getInstance().get(DocumentType.class, invDato.getTransactionDocument().getId());

			Element elmfac = null;

			if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
					|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
				elmfac = docXML.addElement("factura");

				elmfac.addAttribute("id", "comprobante");
				elmfac.addAttribute("version", "1.0.0");

			} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
					|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
				elmfac = docXML.addElement("liquidacionCompra");

				elmfac.addAttribute("id", "comprobante");
				elmfac.addAttribute("version", "1.0.0");

			} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
					|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
				elmfac = docXML.addElement("notaCredito");

				elmfac.addAttribute("id", "comprobante");
				elmfac.addAttribute("version", "1.0.0");

			} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
					|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
				elmfac = docXML.addElement("notaDebito");

				elmfac.addAttribute("id", "comprobante");
				elmfac.addAttribute("version", "1.0.0");

			} else {
				msg.setType("Error");
				String strMensaje = "No existe ese tipo de documento para el SRI, "
						+ "solo existen en está plantilla el " + "<1> Factura, "
						+ "<3> Liquidación de compra de Bienes o Prestación de servicios, " + "<4> Nota de Crédito, "
						+ "<5>  Nota de Débito, su tipo de documento es: "
						+ dctDato.getCoTipoComprobanteAutorizadorSRI()
						+ ", por favor cambie el tipo de documento si quiere facturar con el SRI Electrónicamente";
				msg.setMessage(strMensaje);
				msg.setTitle("@Error@");
				return false;
			}

			final Element elminftri = elmfac.addElement("infoTributaria");

			/**
			 * Agregando Información tributaria
			 */

			if (invDato.getClient() != null) {
				cltDato = OBDal.getInstance().get(Client.class, invDato.getClient().getId());

				Date cldFechaIn = invDato.getInvoiceDate();
				SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");
				SimpleDateFormat sdfFormatoaut = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

				oriDato = OBDal.getInstance().get(OrganizationInformation.class, invDato.getOrganization().getId());

				bspDato = OBDal.getInstance().get(BusinessPartner.class, invDato.getBusinessPartner().getId());

				axmlDirMatriz = ATECOFFGenerarXmlData.methodSelDirMatriz(conn,invDato.getClient().getId());

				String strDirMat = "";
				if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
					strDirMat = axmlDirMatriz[0].dato1;
				}
				
				/********************* 20/02/2020 *********************/
		        String strRegimen = "";
		        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
		        	strRegimen = axmlDirMatriz[0].dato2;
		        }
		        
		        String strAgente = "";
		        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
		        	strAgente = axmlDirMatriz[0].dato3;
		        }
		        /*****************************************************/

				axmlDirec = ATECOFFGenerarXmlData.methodSeleccionarDirec(conn, invDato.getOrganization().getId());

				String strDir = "";
				if (axmlDirec != null && axmlDirec.length == 1) {
					strDir = axmlDirec[0].dato1;
				}

				/**
				 * Verificando Clave de Acceso
				 */
				if (ATECOFF_Operacion_Auxiliares.generarCabecera(elminftri, cltDato.getAtecfeTipoambiente(),
						dctDato.getCoTipoComprobanteAutorizadorSRI().toString(), oriDato.getTaxID(),
						invDato.getCoNroEstab(), invDato.getCoPuntoEmision(), invDato.getDocumentNo(),
						cltDato.getAtecfeCodinumerico(), cltDato.getAtecfeTipoemisi(),
						sdfFormatoClave.format(cldFechaIn),
						ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDirMat),
						ATECOFF_Operacion_Auxiliares.normalizacionPalabras(cltDato.getName()),
						ATECOFF_Operacion_Auxiliares.normalizacionPalabras(cltDato.getName()), 
						strRegimen, strAgente,
						msg, 
						hstClaveAcceso)
						&& dctDato != null && oriDato != null && bspDato != null) {
					Element elminffac = null;
					Element elminPagos = null;
					Element elminPago = null;

					Element elminDetAdi = null;
					Element elmdetAdi = null;

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
						elminffac = elmfac.addElement("infoFactura");
					} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						elminffac = elmfac.addElement("infoLiquidacionCompra");
					} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
						elminffac = elmfac.addElement("infoNotaCredito");
					} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
						elminffac = elmfac.addElement("infoNotaDebito");
					} else {
						msg.setType("Error");
						String strMensaje = "No existe ese tipo de documento para el SRI, "
								+ "solo existen en está plantilla el " + "<1> Factura, "
								+ "<3> Liquidación de compra de Bienes o Prestación de servicios, "
								+ "<4> Nota de Crédito, " + "<5>  Nota de Débito, su tipo de documento es: "
								+ dctDato.getCoTipoComprobanteAutorizadorSRI()
								+ ", por favor cambie el tipo de documento si quiere facturar con el SRI Electrónicamente";
						msg.setMessage(strMensaje);
						msg.setTitle("@Error@");
						return false;
					}

					elminffac.addElement("fechaEmision").addText(sdfFormato.format(cldFechaIn));

					if (!strDir.equals("")) {
						elminffac.addElement("dirEstablecimiento")
								.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDir));
					}

					String strIden = null;

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
						if (bspDato.getCOTipoIdentificacion().toString().equals("01")
								|| bspDato.getCOTipoIdentificacion().toString().equals("1")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("04");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("02")
								|| bspDato.getCOTipoIdentificacion().toString().equals("2")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("05");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("03")
								|| bspDato.getCOTipoIdentificacion().toString().equals("3")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("06");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("07")
								|| bspDato.getCOTipoIdentificacion().toString().equals("7")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("07");
							strIden = "9999999999999";
						} else {
							msg.setType("Error");
							msg.setMessage("El cliente debe tener un tipo de identificacion valido ");
							msg.setTitle("@Error@");
							return false;
						}
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
						if (bspDato.getName2() != null) {
							elminffac.addElement("razonSocialComprador")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName2()));
						} else if (bspDato.getName() != null) {
							elminffac.addElement("razonSocialComprador")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName()));
						} else {
							msg.setType("Error");
							msg.setMessage("Es necesaria la razón social del comprador");
							msg.setTitle("@Error@");
							return false;
						}
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
						if (bspDato.getTaxID() != null) {

							if (strIden == null) {
								strIden = bspDato.getTaxID();
							}

							elminffac.addElement("identificacionComprador").addText(strIden);
						} else {
							msg.setType("Error");
							msg.setMessage("Es necesaria la CI/RUC/Pasaporte");
							msg.setTitle("@Error@");
							return false;
						}
					}

					if (cltDato.getAtecfeNumresolsri() != null) {
						String strNumeReso = cltDato.getAtecfeNumresolsri();
						for (int i = 0; i < (3 - cltDato.getAtecfeNumresolsri().length()); i++) {
							strNumeReso = "0" + strNumeReso;
						}

						if (strNumeReso.length() >= 3 && strNumeReso.length() <= 5) {
							elminffac.addElement("contribuyenteEspecial").addText(strNumeReso);
						} else {
							msg.setType("Error");
							msg.setMessage("El número de contribuyente es de máximo 5 caracteres");
							msg.setTitle("@Error@");
							return false;
						}
					}

					if (cltDato.isAtecfeObligcontabi()) {
						elminffac.addElement("obligadoContabilidad").addText("SI");
					} else {
						elminffac.addElement("obligadoContabilidad").addText("NO");
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
						if (bspDato.getCOTipoIdentificacion().toString().equals("01")
								|| bspDato.getCOTipoIdentificacion().toString().equals("1")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("04");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("02")
								|| bspDato.getCOTipoIdentificacion().toString().equals("2")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("05");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("03")
								|| bspDato.getCOTipoIdentificacion().toString().equals("3")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("06");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("07")
								|| bspDato.getCOTipoIdentificacion().toString().equals("7")) {
							elminffac.addElement("tipoIdentificacionComprador").addText("07");
							strIden = "9999999999999";
						} else {
							msg.setType("Error");
							msg.setMessage("El cliente debe tener un tipo de identificacion valido ");
							msg.setTitle("@Error@");
							return false;
						}
					}

					/* NPI */
					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						if (bspDato.getCOTipoIdentificacion().toString().equals("01")
								|| bspDato.getCOTipoIdentificacion().toString().equals("1")) {
							elminffac.addElement("tipoIdentificacionProveedor").addText("04");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("02")
								|| bspDato.getCOTipoIdentificacion().toString().equals("2")) {
							elminffac.addElement("tipoIdentificacionProveedor").addText("05");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("03")
								|| bspDato.getCOTipoIdentificacion().toString().equals("3")) {
							elminffac.addElement("tipoIdentificacionProveedor").addText("06");
						} else if (bspDato.getCOTipoIdentificacion().toString().equals("07")
								|| bspDato.getCOTipoIdentificacion().toString().equals("7")) {
							elminffac.addElement("tipoIdentificacionProveedor").addText("07");
							strIden = "9999999999999";
						} else {
							msg.setType("Error");
							msg.setMessage("El proveedor debe tener un tipo de identificacion valido ");
							msg.setTitle("@Error@");
							return false;
						}
					}

					if (null != null) {
						elminffac.addElement("guiaRemision").addText(null);
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
						if (bspDato.getName2() != null) {
							elminffac.addElement("razonSocialComprador")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName2()));
						} else if (bspDato.getName() != null) {
							elminffac.addElement("razonSocialComprador")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName()));
						} else {
							msg.setType("Error");
							msg.setMessage("Es necesaria la razón social del comprador");
							msg.setTitle("@Error@");
							return false;
						}
					}

					/* NPI */
					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						if (bspDato.getName() != null) {
							elminffac.addElement("razonSocialProveedor")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName()));
						} else if (bspDato.getName2() != null) {
							elminffac.addElement("razonSocialProveedor")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDato.getName2()));
						} else {
							msg.setType("Error");
							msg.setMessage("Es necesaria la razón social del proveedor");
							msg.setTitle("@Error@");
							return false;
						}
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
						if (bspDato.getTaxID() != null) {

							if (strIden == null) {
								strIden = bspDato.getTaxID();
							}

							elminffac.addElement("identificacionComprador").addText(strIden);
						} else {
							msg.setType("Error");
							msg.setMessage("Es necesaria la CI/RUC/Pasaporte");
							msg.setTitle("@Error@");
							return false;
						}
					}

					/* NPI */
					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						if (bspDato.getTaxID() != null) {

							if (strIden == null) {
								strIden = bspDato.getTaxID();
							}

							elminffac.addElement("identificacionProveedor").addText(strIden);
						} else {
							msg.setType("Error");
							msg.setMessage("Es necesaria la CI/RUC/Pasaporte");
							msg.setTitle("@Error@");
							return false;
						}
					}

					/* NPI */
					String strDirProv = "";
					axmlDirProv = ATECOFFGenerarXmlData.methodSeleccionarDirecProveedor(conn,
							invDato.getPartnerAddress().getId());
					if (axmlDirProv != null && axmlDirProv.length == 1) {
						strDirProv = axmlDirProv[0].dato1;
					}
					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						if (!strDirProv.equals("")) {
							elminffac.addElement("direccionProveedor")
									.addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDirProv));
						}
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {

						if (invDato.getAtecfeCInvoice() != null) {
							Invoice invDatoAux = OBDal.getInstance().get(Invoice.class,
									invDato.getAtecfeCInvoice().getId());

							DocumentType dctDatoAux = OBDal.getInstance().get(DocumentType.class,
									invDatoAux.getTransactionDocument().getId());

							if (dctDatoAux.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
								elminffac.addElement("codDocModificado").addText("01");
							} else if (dctDatoAux.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								elminffac.addElement("codDocModificado").addText("04");
							} else if (dctDatoAux.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
								elminffac.addElement("codDocModificado").addText("05");
							} else {
								msg.setType("Error");
								String strMensaje = "No existe ese tipo de documento de la factura relacionada a la Nota de Crédito"
										+ " para el SRI, solo existen en está plantilla el " + "<1> Factura, "
										+ "<4> Nota de Crédito, " + "<5>  Nota de Débito, su tipo de documento es: "
										+ dctDatoAux.getCoTipoComprobanteAutorizadorSRI()
										+ ", por favor cambie el tipo de documento si quiere facturar con el SRI Electrónicamente";
								msg.setMessage(strMensaje);
								msg.setTitle("@Error@");
								return false;
							}

							String strNrEstraAux = invDatoAux.getCoNroEstab();
							String strNrPuntEmAux = invDatoAux.getCoPuntoEmision();
							String strNrDocAux = invDatoAux.getDocumentNo();

							String strSerie = "";

							if (strNrEstraAux.length() <= 3 && strNrPuntEmAux.length() <= 3) {

								for (int i = 0; i < (3 - strNrEstraAux.length()); i++) {
									strSerie = strSerie + "0";
								}

								strSerie = strSerie + strNrEstraAux + "-";

								for (int i = 0; i < (3 - strNrPuntEmAux.length()); i++) {
									strSerie = strSerie + "0";
								}

								strSerie = strSerie + strNrPuntEmAux + "-";
							}

							if (strNrDocAux.length() <= 9) {
								for (int i = 0; i < (9 - strNrDocAux.length()); i++) {
									strSerie = strSerie + "0";
								}
								strSerie = strSerie + strNrDocAux;
							}

							elminffac.addElement("numDocModificado").addText(strSerie);

							elminffac.addElement("fechaEmisionDocSustento")
									.addText(sdfFormato.format(invDatoAux.getInvoiceDate()));

						} else {
							msg.setType("Error");
							msg.setMessage("La nota de crédito tiene que estar relacionada a una factura");
							msg.setTitle("@Error@");
							return false;
						}
					}

					elminffac.addElement("totalSinImpuestos").addText(invDato.getSummedLineAmount().toString());

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
						elminffac.addElement("valorModificacion").addText(invDato.getGrandTotalAmount().toString());
						elminffac.addElement("moneda").addText("DOLAR");
					}

					Element elmDescTot = null;

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						elmDescTot = elminffac.addElement("totalDescuento");
					}

					/* NPI */
					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {

						if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")) {
							elminffac.addElement("codDocReembolso").addText("03");
						} else {
							elminffac.addElement("codDocReembolso")
									.addText(dctDato.getCoTipoComprobanteAutorizadorSRI().toString());
						}

						double strTotComRee = 0;
						double strBasImpRee = 0;
						double strImpRee = 0;
						axmlTotImpRee = ATECOFFGenerarXmlData.methodSeleccionarTotImpReembolso(conn, invDato.getId());

						strTotComRee = Double.parseDouble(axmlTotImpRee[0].dato1);
						strBasImpRee = Double.parseDouble(axmlTotImpRee[0].dato2);
						strImpRee = Double.parseDouble(axmlTotImpRee[0].dato3);

						elminffac.addElement("totalComprobantesReembolso").addText(String.valueOf(strTotComRee));

						elminffac.addElement("totalBaseImponibleReembolso").addText(String.valueOf(strBasImpRee));

						elminffac.addElement("totalImpuestoReembolso").addText(String.valueOf(strImpRee));
					}

					axmlImpuestos = ATECOFFGenerarXmlData.methodSeleccionarImpues(conn, invDato.getId());

					if (axmlImpuestos != null && axmlImpuestos.length > 0) {
						Element elmtolcimp = null;

						if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
							elmtolcimp = elminffac.addElement("totalConImpuestos");
						} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
							elmtolcimp = elminffac.addElement("impuestos");
						}

						for (int i = 0; i < axmlImpuestos.length; i++) {

							Element elmtolimp = null;

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								elmtolimp = elmtolcimp.addElement("totalImpuesto");
							} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
								elmtolimp = elmtolcimp.addElement("impuesto");
							}

							if (!axmlImpuestos[i].dato1.equals("")) {
								elmtolimp.addElement("codigo").addText(axmlImpuestos[i].dato1);
							} else {
								msg.setType("Error");
								msg.setMessage("El impuesto debe tener un código");
								msg.setTitle("@Error@");
								return false;
							}

							if (!axmlImpuestos[i].dato2.equals("")) {
								elmtolimp.addElement("codigoPorcentaje").addText(axmlImpuestos[i].dato2);
							} else {
								msg.setType("Error");
								msg.setMessage("El impuesto debe tener un código porcentaje");
								msg.setTitle("@Error@");
								return false;
							}

							/* NPI */
							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
								double strDesFac = 0;
								axmlDescuentoFac = ATECOFFGenerarXmlData.methodSeleccionarDescuentoFactura(conn,
										invDato.getId());
								strDesFac = Double.parseDouble(axmlDescuentoFac[0].dato1);
								elmtolimp.addElement("descuentoAdicional").addText(String.valueOf(strDesFac));
							}

							elmtolimp.addElement("baseImponible").addText(axmlImpuestos[i].dato3);

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
								elmtolimp.addElement("tarifa").addText(axmlImpuestos[i].dato5);
							}

							elmtolimp.addElement("valor").addText(axmlImpuestos[i].dato4);
						}
					} else {
						msg.setType("Error");
						msg.setMessage("El documento debe tener detalles");
						msg.setTitle("@Error@");
						return false;
					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
						elminffac.addElement("propina").addText("0.00");
						elminffac.addElement("importeTotal").addText(invDato.getGrandTotalAmount().toString());
						elminffac.addElement("moneda").addText("DOLAR");
						elminPagos = elminffac.addElement("pagos");
						elminPago = elminPagos.addElement("pago");
						elminPago.addElement("formaPago")
								.addText(invDato.getPaymentMethod().getAtsCodigo().toLowerCase());
						elminPago.addElement("total").addText(invDato.getGrandTotalAmount().toString());
						elminPago.addElement("plazo")
								.addText(invDato.getPaymentTerms().getOverduePaymentDaysRule().toString());
						elminPago.addElement("unidadTiempo").addText("dias");

					}

					/* NPI */
					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						elminffac.addElement("importeTotal").addText(invDato.getGrandTotalAmount().toString());
						elminffac.addElement("moneda").addText("DOLAR");
						elminPagos = elminffac.addElement("pagos");
						elminPago = elminPagos.addElement("pago");
						elminPago.addElement("formaPago")
								.addText(invDato.getPaymentMethod().getAtsCodigo().toLowerCase());
						elminPago.addElement("total").addText(invDato.getGrandTotalAmount().toString());
						elminPago.addElement("plazo")
								.addText(invDato.getPaymentTerms().getOverduePaymentDaysRule().toString());
						elminPago.addElement("unidadTiempo").addText("dias");

					}

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
						if (invDato.getDescription() != null) {
							if (!invDato.getDescription().equals("")) {
								elminffac.addElement("motivo").addText(
										ATECOFF_Operacion_Auxiliares.normalizacionPalabras(invDato.getDescription()));
							} else {
								msg.setType("Error");
								msg.setMessage("Por favor ingrese el motivo de la nota de crédito en la "
										+ "descripción de la nota de crédito, caso contrario no se "
										+ "podrá autorizar por medio del SRI");
								msg.setTitle("@Error@");
								return false;
							}
						} else {
							msg.setType("Error");
							msg.setMessage("Por favor ingrese el motivo de la nota de crédito en la "
									+ "descripción de la nota de crédito, caso contrario no se "
									+ "podrá autorizar por medio del SRI");
							msg.setTitle("@Error@");
							return false;
						}
					}

					axmlDetalles = ATECOFFGenerarXmlData.methodSelecDetallesLiqui(conn, invDato.getId());

					double intTotalDesc = 0;

					Element elmdetimps = null;
					// Element elmdeladics = null;

					if (axmlDetalles != null && axmlDetalles.length > 0) {
						Element elmdetfac = null;
						if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
							elmdetfac = elmfac.addElement("detalles");
						} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
							elmdetfac = elmfac.addElement("motivos");
						}

						for (int i = 0; i < axmlDetalles.length; i++) {
							Element elmdeta = null;

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								elmdeta = elmdetfac.addElement("detalle");
							} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
								elmdeta = elmdetfac.addElement("motivo");
							}

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								if (axmlDetalles[i].dato1.length() > 25) {
									msg.setType("Error");
									msg.setMessage(
											"El identificador del producto debe tener máximo 25 caracteres, su identificador de producto es: "
													+ axmlDetalles[i].dato1 + " y longitud de "
													+ axmlDetalles[i].dato1.length());
									msg.setTitle("@Error@");
									return false;
								}
							}

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
								elmdeta.addElement("codigoPrincipal").addText(
										ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato1));
							} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								elmdeta.addElement("codigoInterno").addText(
										ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato1));
							}

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								if (!axmlDetalles[i].dato2.equals("")) {
									elmdeta.addElement("codigoAuxiliar").addText(
											ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato2));
								}
								if (!axmlDetalles[i].dato3.equals("")) {
									elmdeta.addElement("descripcion").addText(
											ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato3));
								} else {
									msg.setType("Error");
									msg.setMessage("Los detalles tienen que tener una descripción");
									msg.setTitle("@Error@");
									return false;
								}
							} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
								if (!axmlDetalles[i].dato3.equals("")) {
									elmdeta.addElement("razon").addText(
											ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato3));
								}
							}

							/*
							 * Agregando información del detalle
							 */
							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
								elmdeta.addElement("cantidad").addText(axmlDetalles[i].dato4);
								elmdeta.addElement("precioUnitario").addText(axmlDetalles[i].dato5);
								elmdeta.addElement("descuento").addText(axmlDetalles[i].dato6);

								intTotalDesc = intTotalDesc + Double.parseDouble(axmlDetalles[i].dato6);

								elmdeta.addElement("precioTotalSinImpuesto").addText(axmlDetalles[i].dato7);

							} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
								elmdeta.addElement("valor").addText(axmlDetalles[i].dato7);
							}

							/* NPI */
							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
								elmdeta.addElement("unidadMedida").addText(axmlDetalles[i].dato9);
								elmdeta.addElement("cantidad").addText(axmlDetalles[i].dato4);
								elmdeta.addElement("precioUnitario").addText(axmlDetalles[i].dato5);
								elmdeta.addElement("descuento").addText(axmlDetalles[i].dato6);

								intTotalDesc = intTotalDesc + Double.parseDouble(axmlDetalles[i].dato6);

								elmdeta.addElement("precioTotalSinImpuesto").addText(axmlDetalles[i].dato7);

								elminDetAdi = elmdeta.addElement("detallesAdicionales");
								elmdetAdi = elminDetAdi.addElement("detAdicional");
								elmdetAdi.addAttribute("nombre", "nombre0");
								elmdetAdi.addAttribute("valor", "valor0");
								elmdetAdi = elminDetAdi.addElement("detAdicional");
								elmdetAdi.addAttribute("nombre", "nombre1");
								elmdetAdi.addAttribute("valor", "valor1");

							}

							axmldetImps = ATECOFFGenerarXmlData.methodSeleccionarDetalTax(conn, axmlDetalles[i].dato8);

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {

								elmdetimps = elmdeta.addElement("impuestos");

								if (axmldetImps != null && axmldetImps.length > 0) {

									for (int j = 0; j < axmldetImps.length; j++) {
										Element elmdetimp = elmdetimps.addElement("impuesto");

										if (!axmldetImps[j].dato1.equals("")) {
											elmdetimp.addElement("codigo").addText(axmldetImps[j].dato1);
										} else {
											msg.setType("Error");
											msg.setMessage("El impuesto debe tener un código");
											msg.setTitle("@Error@");
											return false;
										}

										if (!axmldetImps[j].dato2.equals("")) {
											elmdetimp.addElement("codigoPorcentaje").addText(axmldetImps[j].dato2);
										} else {
											msg.setType("Error");
											msg.setMessage("El impuesto debe tener un código de porcentaje");
											msg.setTitle("@Error@");
											return false;
										}

										if (!axmldetImps[j].dato3.equals("")) {
											elmdetimp.addElement("tarifa").addText(axmldetImps[j].dato3);
										} else {
											msg.setType("Error");
											msg.setMessage("El impuesto debe tener una tarifa");
											msg.setTitle("@Error@");
											return false;
										}

										elmdetimp.addElement("baseImponible").addText(axmldetImps[j].dato4);
										elmdetimp.addElement("valor").addText(axmldetImps[j].dato5);
									}
								}
							}
						}
					} else {
						msg.setType("Error");
						msg.setMessage("El documento debe tener detalles");
						msg.setTitle("@Error@");
						return false;
					}

					BigDecimal bd = new BigDecimal(intTotalDesc).setScale(2, RoundingMode.HALF_UP);
					double totaldescuento = bd.doubleValue();

					if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
						elmDescTot.setText(String.valueOf(totaldescuento));
					} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
							|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
						elminffac.addElement("valorTotal").addText(invDato.getGrandTotalAmount().toString());
					}

					/* NPI */
					axmlReembolsos = ATECOFFGenerarXmlData.methodSeleccionarReembolsos(conn, invDato.getId());

					if (axmlReembolsos != null && axmlReembolsos.length > 0) {
						Element elmReem = null;
						if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
								|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
							elmReem = elmfac.addElement("reembolsos");
						}

						for (int i = 0; i < axmlReembolsos.length; i++) {
							Element elmDetaReem = null;

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
								elmDetaReem = elmReem.addElement("reembolsoDetalle");
							}

							if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
									|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {

								String strIdenReem = null;

								if (axmlReembolsos[i].dato1.toString().equals("01")
										|| axmlReembolsos[i].dato1.toString().equals("1")) {
									elmDetaReem.addElement("tipoIdentificacionProveedorReembolso").addText("04");
								} else if (axmlReembolsos[i].dato1.toString().equals("02")
										|| axmlReembolsos[i].dato1.toString().equals("2")) {
									elmDetaReem.addElement("tipoIdentificacionProveedorReembolso").addText("05");
								} else if (axmlReembolsos[i].dato1.toString().equals("03")
										|| axmlReembolsos[i].dato1.toString().equals("3")) {
									elmDetaReem.addElement("tipoIdentificacionProveedorReembolso").addText("06");
								} else if (axmlReembolsos[i].dato1.toString().equals("07")
										|| axmlReembolsos[i].dato1.toString().equals("7")) {
									elmDetaReem.addElement("tipoIdentificacionProveedorReembolso").addText("07");
									strIdenReem = "9999999999999";
								} else {
									msg.setType("Error");
									msg.setMessage("El proveedor debe tener un tipo de identificacion valido ");
									msg.setTitle("@Error@");
									return false;
								}

								if (axmlReembolsos[i].dato1 != null) {

									if (strIdenReem == null) {
										strIdenReem = axmlReembolsos[i].dato2;
									}

									elmDetaReem.addElement("identificacionProveedorReembolso").addText(strIdenReem);
								} else {
									msg.setType("Error");
									msg.setMessage("Es necesaria la CI/RUC/Pasaporte");
									msg.setTitle("@Error@");
									return false;
								}

								elmDetaReem.addElement("codPaisPagoProveedorReembolso")
										.addText(axmlReembolsos[i].dato3);

								if (axmlReembolsos[i].dato1.toString().equals("01")
										|| axmlReembolsos[i].dato1.toString().equals("1")) {
									elmDetaReem.addElement("tipoProveedorReembolso").addText("02");
								} else {
									elmDetaReem.addElement("tipoProveedorReembolso").addText("01");
								}

								if (!axmlReembolsos[i].dato4.toString().equals(null)) { /* Tipo de documento del reembolso, lineas */

									elmDetaReem.addElement("codDocReembolso").addText(axmlReembolsos[i].dato4);

									elmDetaReem.addElement("estabDocReembolso").addText(axmlReembolsos[i].dato5);
									elmDetaReem.addElement("ptoEmiDocReembolso").addText(axmlReembolsos[i].dato6);
									elmDetaReem.addElement("secuencialDocReembolso").addText(axmlReembolsos[i].dato7);
									elmDetaReem.addElement("fechaEmisionDocReembolso").addText(axmlReembolsos[i].dato8);
									elmDetaReem.addElement("numeroautorizacionDocReemb")
											.addText(axmlReembolsos[i].dato9);

									axmlImpReem = ATECOFFGenerarXmlData.methodSeleccionarImpRee(conn,
											invDato.getClient().getId(), axmlReembolsos[i].dato10);
									Element elmDetaImps = null;

									if (axmlImpReem != null && axmlImpReem.length > 0) {
										elmDetaImps = elmDetaReem.addElement("detalleImpuestos");

										for (int j = 0; j < axmlImpReem.length; j++) {
											Element elmDetaImp = elmDetaImps.addElement("detalleImpuesto");

											if (!axmlImpReem[j].dato1.equals("")) {
												elmDetaImp.addElement("codigo").addText(axmlImpReem[j].dato1);

											} else {
												msg.setType("Error");
												msg.setMessage("El impuesto debe tener un código");
												msg.setTitle("@Error@");
												return false;
											}

											if (!axmlImpReem[j].dato2.equals("")) {
												elmDetaImp.addElement("codigoPorcentaje").addText(axmlImpReem[j].dato2);
											} else {
												msg.setType("Error");
												msg.setMessage("El impuesto debe tener un código de la tarifa");
												msg.setTitle("@Error@");
												return false;
											}

											if (!axmlImpReem[j].dato3.equals("")) {
												elmDetaImp.addElement("tarifa")
														.addText(String.valueOf(axmlImpReem[j].dato3));
											} else {
												msg.setType("Error");
												msg.setMessage("El impuesto debe tener una tarifa");
												msg.setTitle("@Error@");
												return false;
											}

											elmDetaImp.addElement("baseImponibleReembolso")
													.addText(axmlImpReem[j].dato4);
											elmDetaImp.addElement("impuestoReembolso").addText(axmlImpReem[j].dato5);
										}
									} else {
										msg.setType("Error");
										msg.setMessage("El Reembolso no tiene impuestos");
										msg.setTitle("@Error@");
										return false;
									}

									/* Verifica si la base imponible y el impuesto del Reembolso son correctos */

									axmlVerificarReem = ATECOFFGenerarXmlData.methodTotales(conn, invDato.getId());

									if (!axmlVerificarReem[0].dato1.equals(axmlVerificarReem[0].dato3)) {
										msg.setType("Error");
										msg.setMessage("La Base Imponible del Reembolso es incorrecta");
										msg.setTitle("@Error@");
										return false;
									}

									if (!axmlVerificarReem[0].dato2.equals(axmlVerificarReem[0].dato4)) {
										msg.setType("Error");
										msg.setMessage("El Impuesto del Reembolso es incorrecto");
										msg.setTitle("@Error@");
										return false;
									}
									/**/

								} else {
									msg.setType("Error");
									String strMensaje = "Por favor ingrese el tipo de documento del Reembolso.";
									msg.setMessage(strMensaje);
									msg.setTitle("@Error@");
									return false;
								}
							}
						}
					}

					ATECOFF_Operacion_Auxiliares.addCamposAdic(elmfac, conn, invDato.getClient().getId(), invDato.getId());

					final XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flXml), "utf-8"),
							ofFormat);
					writer.write(docXML);
					writer.flush();
					writer.close();

					flXml = ATECOFF_Operacion_Auxiliares.firmarDocumento(flXml, conn, strUser, invDato.getClient().getId(), msg);

					axmlMinout = ATECOFFGenerarXmlData.methodVerificarGuia(conn, invDato.getId());

					String strMinout = null;

					if (axmlMinout.length > 0 && !enviarSRI) {
						strMinout = axmlMinout[0].dato1;
						ShipmentInOut spiDato = OBDal.getInstance().get(ShipmentInOut.class, strMinout);

						if (strMinout != null) {
							if (!ATECOFF_GenerarGuiaDespachoXML.generarFacturaXMLGre(spiDato, conn, strUser, enviarSRI,
									msg)) {
								return false;
							}
						}
					}

					if (flXml != null) {

						if (enviarSRI) {
							Calendar cldFecha = new GregorianCalendar();
							String strFechaAut = "";

							strFechaAut = sdfFormatoaut.format(cldFecha.getTime());

							byte[] bytes = ATECOFF_Operacion_Auxiliares.filetobyte(flXml);
							fileString = new String(bytes, "UTF-8");

							String mensaje = "";

							ATECOFF_SRIDocumentoAutorizado autorizadoPre = new ATECOFF_SRIDocumentoAutorizado();
							ATECOFF_SRIDocumentoRecibido recibido = new ATECOFF_SRIDocumentoRecibido();

							ATECOFF_ServiceAutorizacion serviceAutorizacionPre = new ATECOFF_ServiceAutorizacion(
									cltDato.getAtecfeTipoambiente(), hstClaveAcceso.get("claveacc"));

							autorizadoPre = serviceAutorizacionPre.CallAutorizado();

							if (!autorizadoPre.getEstadoespecifico().equals("AUT")) {
								ATECOFF_ServiceRecibido serviceRecibido = new ATECOFF_ServiceRecibido(
										cltDato.getAtecfeTipoambiente(), fileString);
								recibido = serviceRecibido.CallRecibido();
							} else {
								recibido.setEstado("RECIBIDO");
								recibido.setEstadoespecifico("REC");
							}

							if (recibido.getInformacion() != null) {
								mensaje = recibido.getMensaje() + " - " + recibido.getInformacion().replace("'", "");
							}

							if (recibido.getEstadoespecifico().equals("REC")) {
								ATECOFF_SRIDocumentoAutorizado autorizado = new ATECOFF_SRIDocumentoAutorizado();
								ATECOFF_ServiceAutorizacion serviceAutorizacion = new ATECOFF_ServiceAutorizacion(
										cltDato.getAtecfeTipoambiente(), hstClaveAcceso.get("claveacc"));

								// hstClaveAcceso.get("claveacc")
								autorizado = serviceAutorizacion.CallAutorizado();

								if (autorizado.getEstadoespecifico().equals("AUT")) {
									if (autorizado.getInformacion() != null) {
										mensaje = autorizado.getMensaje() + " - "
												+ autorizado.getInformacion().replace("'", "");
									}
									
									/********************* YRO 14/09/2021 **********************/
					                 ATECOFFGenerarXmlData.methodActualizaAutFact(conn, autorizado.getFechaAutorizacion().toString(),invDato.getId());
					                /***********************************************************/

									List<File> lisdoc = new ArrayList<File>();
									lisdoc.add(autorizado.getDocFile());

									File flPdf = null;

									if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("1")
											|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("01")) {
										flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
												"@basedesign@/com/atrums/felectronica/erpReport/Rpt_Factura.jrxml",
												"Factura", invDato.getId());
									} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("3")
											|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("03")) {
										flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
												"@basedesign@/com/atrums/felectronica/erpReport/Rpt_Liquidacion_Compra.jrxml",
												"Liquidacion_Compra", invDato.getId());
									} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("4")
											|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("04")) {
										flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
												"@basedesign@/com/atrums/felectronica/erpReport/Rpt_NotaCredito.jrxml",
												"Nota_Credito", invDato.getId());
									} else if (dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("5")
											|| dctDato.getCoTipoComprobanteAutorizadorSRI().toString().equals("05")) {
										flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
												"@basedesign@/com/atrums/felectronica/erpReport/Rpt_NotaDebito.jrxml",
												"Nota_Debito", invDato.getId());
									}

									if (flPdf != null)
										lisdoc.add(flPdf);

									axmlEmail = ATECOFFGenerarXmlData.methodSeleccionarEmail(conn,
											invDato.getBusinessPartner().getId());

									String strSubject = "Liquidación de Compra Electrónica de " + axmlEmail[0].dato2;

									String strContenido = "";

									
									/*strContenido = "Señor/a\n" + axmlEmail[0].dato3
											+ "\n\nUd tiene un documento electrónico que puede ser consultada en: "
											+ axmlEmail[0].dato4 + "\nCon los credeciales: \n\n -Usuario: "
											+ axmlEmail[0].dato5 + "\n -Contraseña: " + axmlEmail[0].dato5
											+ "\n\n\nAtentamente " + axmlEmail[0].dato2;*/
									
 									String nomDoc = "Liquidación de Compra Electrónica";
					                	type = "text/html; charset=utf-8";
					                    strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
					                    		"	<tr style=\"background-color: #003764\">\r\n" + 
					                    		"		<td style=\"width: 85%\">\r\n" + 
					                    		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
					                    		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
					                    		"			</a></td>\r\n" + 
					                    		"		<td style=\"width: 3%\">\r\n" + 
					                    		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
					                    		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
					                    		"			</a></td>\r\n" + 
					                    		"        <td style=\"width: 3%\">		\r\n" + 
					                    		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
					                    		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
					                    		"			</a></td>\r\n" + 
					                    		"        <td style=\"width: 3%\">		\r\n" + 
					                    		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
					                    		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
					                    		"			</a></td>\r\n" + 
					                    		"        <td style=\"width: 3%\">	\r\n" + 
					                    		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
					                    		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
					                    		"			</a></td>	\r\n" + 
					                    		"		<td style=\"width: 3%\">\r\n" + 
					                    		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
					                    		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
					                    		"			</a></td>\r\n" + 
					                    		"	</tr>\r\n" + 
					                    		"	<tr>\r\n" + 
					                    		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
					                    		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
					                    		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
					                    		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
					                    		"				<p><b>"+ axmlEmail[0].dato3 +"</b></p>\r\n" + 
					                    		"				<p>Usted tiene una "+ nomDoc +" generada por la empresa <b>"+ axmlEmail[0].dato2 +".</b></p>\r\n" + 
					                    		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
					                    		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
					                    		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
					                    		"				</div>	\r\n" + 
					                    		"				<div style=\"font-size: 12px\">\r\n" + 
					                    		"					<p style=\"margin-top:50px\">\r\n" + 
					                    		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
					                    		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
					                    		"					</p>\r\n" + 
					                    		"					<p>\r\n" + 
					                    		"					   Recuerde que el documento electrónico cumple con todas las disposiciones establecidas en el marco legal vigente y sustituye al documento en formato impreso con igual valor legal.<br> \r\n" + 
					                    		"					   Le recomendamos no imprimir este correo electrónico a menos que sea estrictamente necesario.\r\n" + 
					                    		"					</p>\r\n" + 
					                    		"					<p>\r\n" + 
					                    		"					   Por favor, no responda a este correo electrónico.\r\n" + 
					                    		"					<p>\r\n" + 
					                    		"				</div>\r\n" + 
					                    		"			</div>\r\n" + 
					                    		"		  </div>\r\n" + 
					                    		"		</td>\r\n" + 
					                    		"	</tr>\r\n" + 
					                    		"	<tr>\r\n" + 
					                    		"	  <td colspan=\"6\">\r\n" + 
					                    		"		<table style=\"width: 100%\">\r\n" + 
					                    		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
					                    		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
					                    		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
					                    		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
					                    		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
					                    		"						</div>\r\n" + 
					                    		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
					                    		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
					                    		"						</div>\r\n" + 
					                    		"						<div style=\"font-size: 12px\">\r\n" + 
					                    		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
					                    		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
					                    		"						</div>\r\n" + 
					                    		"					</div>\r\n" + 
					                    		"				</td>\r\n" + 
					                    		"			</tr>\r\n" + 
					                    		"		</table>\r\n" + 
					                    		"	  </td>\r\n" + 
					                    		"	</tr>\r\n" + 
					                    		"</table>";

									String strMensaje = "Su documento ha sido autorizado por el SRI, ";

									if (ATECOFF_Operacion_Auxiliares.enviarCorreo(axmlEmail[0].dato1, strSubject,
											strContenido, type, lisdoc, false)) {
										msg.setType("Success");
										msg.setTitle("Mensaje");
										msg.setMessage(strMensaje + "y fue enviado al correo electrónico del cliente");
										flXml.delete();

										mensaje = mensaje.equals("") ? "Se envió el email cliente, AUTORIZADO"
												: ", Se envió el email al cliente, AUTORIZADO";

										ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, autorizado.getDocXML(),
												hstClaveAcceso.get("claveacc"), mensaje, "PD", "AP", "PD", "AP",
												hstClaveAcceso.get("claveacc"), autorizado.getFechaAutorizacion().toString(), strFechaAut, invDato.getId()); //YRO 14-09-2021
										return true;
									} else {
										mensaje = mensaje.equals("") ? "No se envió el email cliente, AUTORIZADO"
												: ", No se envió el email al cliente, AUTORIZADO";

										ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, autorizado.getDocXML(),
												hstClaveAcceso.get("claveacc"), mensaje, "PD", "AP", "PD", "AP",
												hstClaveAcceso.get("claveacc"), autorizado.getFechaAutorizacion().toString(), strFechaAut, invDato.getId()); //YRO 14-09-2021

										return true;
									}
								} else {
									if (autorizado.getEstadoespecifico().equals("N")) {
										ATECOFFGenerarXmlData.methodActualizarInvEstadoOffline(conn, "PD",
												invDato.getId());

										msg.setType("Error");
										msg.setMessage(mensaje);
										msg.setTitle("@Error@");
										return false;
									}

									if (autorizado.getInformacion() != null) {
										mensaje = autorizado.getMensaje() + " - "
												+ autorizado.getInformacion().replace("'", "");
									}

									String strSubject = null;

									if (cltDato.getAtecfeTipoambiente().equals("1")) {
										strSubject = "Documento Electrónico Rechazada Ambiente Pruebas";
									} else {
										strSubject = "Documento Electrónico Rechazada Ambiente Producción";
									}

									String strContenido = "Estimado hay un error en el documento factura de la empresa "
											+ cltDato.getName() + ": " + invDato.getDocumentNo()
											+ " con clave de acceso " + hstClaveAcceso.get("claveacc")
											+ "\nEl error es: " + mensaje;

									/*
									 * if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailSoporte, strSubject,
									 * strContenido, null, null, true)) {
									 */

									if (true) {
										ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, autorizado.getDocXML(),
												hstClaveAcceso.get("claveacc"), mensaje, "PD", "RZ", "PD", "RZ",
												hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut,
												invDato.getId());

										msg.setType("Error");
										msg.setMessage(mensaje);
										msg.setTitle("@Error@");
										return false;
									} else {
										ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, autorizado.getDocXML(),
												hstClaveAcceso.get("claveacc"), mensaje, "PD", "PD", "PD", "PD",
												hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut,
												invDato.getId());
										return false;
									}
								}
							} else {

								String strSubject = null;

								if (cltDato.getAtecfeTipoambiente().equals("1")) {
									strSubject = "Documento Electrónico Rechazada Ambiente Pruebas";
								} else {
									strSubject = "Documento Electrónico Rechazada Ambiente Producción";
								}

								String strContenido = "Estimado hay un error en el documento factura de la empresa "
										+ cltDato.getName() + ": " + invDato.getDocumentNo() + " con clave de acceso "
										+ hstClaveAcceso.get("claveacc") + "\nEl error es: " + mensaje;

								/*
								 * if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailSoporte, strSubject,
								 * strContenido, null, null, true)) {
								 */

								if (true) {
									ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, null,
											hstClaveAcceso.get("claveacc"), mensaje, "PD", "RZ", "PD", "RZ",
											hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut, invDato.getId());

									msg.setType("Error");
									msg.setMessage(mensaje);
									msg.setTitle("@Error@");
									return false;
								} else {
									ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, null,
											hstClaveAcceso.get("claveacc"), mensaje, "PD", "PD", "PD", "PD",
											hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut, invDato.getId());
									return false;
								}
							}

						} else {
							Calendar cldFecha = new GregorianCalendar();
							String strFechaAut = "";

							strFechaAut = sdfFormatoaut.format(cldFecha.getTime());

							ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, null,
									hstClaveAcceso.get("claveacc"), null, "PD", "PD", "PD", "PD",
									hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut, invDato.getId());

							msg.setType("Success");
							msg.setTitle("Mensaje");
							msg.setMessage("Documento Procesado Electrónicamente");
							flXml.delete();

							return true;
						}
					} else {
						return false;
					}

				} else {
					return false;
				}
			}

			msg.setType("Error");
			msg.setMessage("No hay un tercero en el documento");
			msg.setTitle("@Error@");
			return false;
		} finally {
			flXml.delete();
			flXml = null;
			fileString = null;

			docXML.clearContent();
			docXML = null;
			ofFormat = null;

			dctDato = null;
			cltDato = null;
			oriDato = null;
			bspDato = null;

			axmlDirMatriz = null;
			axmlDirec = null;
			axmlImpuestos = null;
			axmlDetalles = null;
			axmldetImps = null;
			axmlMinout = null;
			axmlEmail = null;

			hstClaveAcceso.clear();
		}
	}
}
