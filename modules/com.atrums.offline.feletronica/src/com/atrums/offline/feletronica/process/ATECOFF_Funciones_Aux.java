package com.atrums.offline.feletronica.process;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.servlet.ServletException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.xmlrpc.XmlRpcClient;
import org.dom4j.Element;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.utils.FormatUtilities;

import com.atrums.felectronica.data.ATECFE_conf_firma;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class ATECOFF_Funciones_Aux {

  static Logger log4j = Logger.getLogger(ATECOFF_Funciones_Aux.class);

  /*
   * Función para transformar de file a byte 64
   */
  public byte[] filetobyte(File flFile) throws IOException {
    byte[] bytes = new byte[(int) flFile.length()];

    FileInputStream is = new FileInputStream(flFile);

    is.read(bytes);
    is.close();

    byte[] bytesen = Base64.encodeBase64(bytes);
    return bytesen;
  }

  /*
   * Función para transformar de byte 64 a file
   */
  public File bytetofile(byte[] bytes) throws IOException {
    File fldocumen = File.createTempFile("documento", ".xml", null);
    fldocumen.deleteOnExit();
    byte[] bytesde = Base64.decodeBase64(bytes);

    FileOutputStream os = new FileOutputStream(fldocumen);

    os.write(bytesde);
    os.close();

    return fldocumen;
  }

 
  public boolean enviarCorreo(final String recipient, final String subject, final String body,
      final String type, final List<File> attachments) {
    try {
      Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
      final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

      if (mailConfig == null) {
        return false;
      }

      final String username = mailConfig.getSmtpServerAccount();
      final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(),
          false);
      final String connSecurity = mailConfig.getSmtpConnectionSecurity();
      final int port = mailConfig.getSmtpPort().intValue();
      final String senderAddress = mailConfig.getSmtpServerSenderAddress();
      final String host = mailConfig.getSmtpServer();
      final boolean auth = mailConfig.isSMTPAuthentification();

      EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress,
          recipient, null, null, null, subject, body, type, attachments, null, null);
      return true;
    } catch (Exception e) {
      // TODO: handle exception
      return false;
    }
  }

    
  /*
   * Función para firmar el documento electronicamente
   */
  

  public String basedesign(String strReporte) {

    String strDirectorio = ATECOFF_Funciones_Aux.class.getResource("/").getPath();

    if (strDirectorio.indexOf("/C:") != -1 || strDirectorio.indexOf("/c:") != -1) {
      int intPos = strDirectorio.indexOf("/");

      if (intPos != -1) {
        strDirectorio = strDirectorio.substring(strDirectorio.indexOf("/") + 1);
      }
    }

    String strDirectorioInv = "";

    for (int i = 0; i < strDirectorio.length(); i++) {
      strDirectorioInv = strDirectorio.charAt(i) + strDirectorioInv;
    }

    int intPosicion = strDirectorioInv.indexOf("sessalc/dliub");

    if (intPosicion != -1) {
      strDirectorioInv = strDirectorioInv.substring(intPosicion + 13);
    } else {
      intPosicion = strDirectorioInv.indexOf("sessalc/FNI-BEW");
      if (intPosicion != -1) {
        strDirectorioInv = strDirectorioInv.substring(intPosicion + 15);
      } else {
        return "";
      }
    }

    strDirectorio = "";

    for (int i = 0; i < strDirectorioInv.length(); i++) {
      strDirectorio = strDirectorioInv.charAt(i) + strDirectorio;
    }

    String strBaseDesign = strReporte.replaceAll("@basedesign@",
        (strDirectorio + "WebContent/src-loc/design"));

    File flRep = new File(strBaseDesign);

    if (flRep.exists()) {
      return strDirectorio + "WebContent/src-loc/design";
    } else {
      strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "src-loc/design"));

      flRep = new File(strBaseDesign);

      if (flRep.exists()) {
        return strDirectorio + "src-loc/design";
      } else {
        return "";
      }
    }
  }

  public File generarPDF(ConnectionProvider conn, String strBaseDesin, String strNombre,
      String strEntiID) throws ServletException {

    File flTemp = null;

    try {

      String strBaseDesign = strBaseDesin;

      if (!basedesign(strBaseDesign).equals("")) {

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("DOCUMENT_ID", strEntiID);
        parameters.put("BASE_DESIGN", basedesign(strBaseDesign));

        strBaseDesign = strBaseDesign.replaceAll("@basedesign@", basedesign(strBaseDesign));

        JasperReport jasperRepo = JasperCompileManager.compileReport(strBaseDesign);

        Connection con = conn.getTransactionConnection();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, con);

        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
        String strFecha = ft.format(date);
        String strNombreArch = strNombre + "-" + strFecha;
        OutputStream out = null;

        flTemp = File.createTempFile(strNombreArch, ".pdf", null);

        out = new FileOutputStream(flTemp);
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, out);
        out.write(byteout.toByteArray());
        out.flush();
        out.close();
      }

    } catch (JRException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoConnectionAvailableException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
    return flTemp;
  }

  public String normalizacionPalabras(String strPalabras) {

    String strPalabraAux = "";

    strPalabraAux = strPalabras.replaceAll("á", "a");
    strPalabraAux = strPalabraAux.replaceAll("é", "e");
    strPalabraAux = strPalabraAux.replaceAll("í", "i");
    strPalabraAux = strPalabraAux.replaceAll("ó", "o");
    strPalabraAux = strPalabraAux.replaceAll("ú", "u");
    return strPalabraAux;
  }
}