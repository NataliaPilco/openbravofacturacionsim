/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2011-2016 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  Mallikarjun M
 ************************************************************************
 */
package com.atrums.clonar.factura.process;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.openbravo.base.exception.OBException;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.invoice.InvoiceLine;
import org.openbravo.model.pricing.pricelist.PriceListVersion;
import org.openbravo.service.db.CallStoredProcedure;
import org.openbravo.service.json.DataResolvingMode;
import org.openbravo.service.json.DataToJsonConverter;

/**
 * When user on the Sales Order window and have a Sales Order displayed / selected, you then click a
 * button on the toolbar (where the 'new' order button is, among other buttons) called 'Clone
 * Order'. The process would then create a new order, and copy the information from the old order to
 * the new one.
 * 
 * @author Mallikarjun M
 * 
 */
public class CloneInvoiceActionHandler extends BaseActionHandler {

  protected JSONObject execute(Map<String, Object> parameters, String data) {
    final DataToJsonConverter jsonConverter = new DataToJsonConverter();
    JSONObject json = null;
    try {
      String invoiceId = (String) parameters.get("recordId");
      User currentUser = OBContext.getOBContext().getUser();
      Invoice objInvoice = OBDal.getInstance().get(Invoice.class, invoiceId);
      Invoice objCloneInvoice = (Invoice) DalUtil.copy(objInvoice, false);

      objCloneInvoice.setDocumentAction("CO");
      objCloneInvoice.setDocumentStatus("DR");
      objCloneInvoice.setPosted("N");
      objCloneInvoice.setProcessed(false);
      objCloneInvoice.setSalesTransaction(true);
      objCloneInvoice.setDocumentNo(null);
      objCloneInvoice.setSalesTransaction(objInvoice.isSalesTransaction());
      objCloneInvoice.setCreationDate(new Date());
      objCloneInvoice.setUpdated(new Date());
      objCloneInvoice.setCreatedBy(currentUser);
      objCloneInvoice.setUpdatedBy(currentUser);
      objCloneInvoice.setCoNroAutSri(null);
      objCloneInvoice.setAtecfeCodigoAcc(null);
      objCloneInvoice.setCoVencimientoAutSri(null);
      objCloneInvoice.setAtecfeMensajeSri(null);
      objCloneInvoice.setAtecfeDocstatus("PD");
      objCloneInvoice.setAtecfeDocaction("PR");
      objCloneInvoice.setAtecoffDocstatus("DR");
      objCloneInvoice.setAtecoffDocaction("PR");
      objCloneInvoice.setSalesOrder(null);
      objCloneInvoice.setTaxDate(null);
      // Setting order date and scheduled delivery date of header and the order lines to current
      // date to avoid issues with tax rates. Refer issue
      // https://issues.openbravo.com/view.php?id=23671
      Date date = new Date();
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      cal.set(Calendar.HOUR_OF_DAY, 0);
      cal.set(Calendar.MINUTE, 0);
      cal.set(Calendar.SECOND, 0);
      cal.set(Calendar.MILLISECOND, 0);
      objCloneInvoice.setInvoiceDate(cal.getTime());
      objCloneInvoice.setAccountingDate(cal.getTime());
      objCloneInvoice.setGrandTotalAmount(BigDecimal.ZERO);
      objCloneInvoice.setSummedLineAmount(BigDecimal.ZERO);

      // save the cloned order object
      OBDal.getInstance().save(objCloneInvoice);

      // get the lines associated with the invoice and clone them to the new
      // invoice line.
      for (InvoiceLine invLine : objInvoice.getInvoiceLineList()) {
          String strPriceVersionId = getPriceListVersion(objInvoice.getPriceList().getId(), objInvoice
              .getClient().getId());
          BigDecimal bdPriceList = getPriceList(invLine.getProduct().getId(), strPriceVersionId);
          InvoiceLine objCloneInvLine = (InvoiceLine) DalUtil.copy(invLine, false);
          objCloneInvLine.setInvoicedQuantity(invLine.getInvoicedQuantity());
          if (bdPriceList != null && bdPriceList.compareTo(BigDecimal.ZERO) != 0) {
        	  objCloneInvLine.setListPrice(bdPriceList);
          }
          objCloneInvLine.setInvoice(objCloneInvoice);
          objCloneInvLine.setCreationDate(new Date());
          objCloneInvLine.setUpdated(new Date());
          objCloneInvLine.setCreatedBy(currentUser);
          objCloneInvLine.setUpdatedBy(currentUser);
          objCloneInvLine.setSalesOrderLine(null);
          objCloneInvLine.setGoodsShipmentLine(null);
          objCloneInvoice.getInvoiceLineList().add(objCloneInvLine);
          objCloneInvLine.setInvoice(objCloneInvoice);
        }

      OBDal.getInstance().save(objCloneInvoice);

      OBDal.getInstance().flush();
      OBDal.getInstance().refresh(objCloneInvoice);
      json = jsonConverter.toJsonObject(objCloneInvoice, DataResolvingMode.FULL);
      OBDal.getInstance().commitAndClose();
      return json;
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  private String getPriceListVersion(String priceList, String clientId) {
    try {
      String whereClause = " as plv left outer join plv.priceList pl where plv.active='Y' and plv.active='Y' and "
          + " pl.id = :priceList and plv.client.id = :clientId order by plv.validFromDate desc";

      OBQuery<PriceListVersion> ppriceListVersion = OBDal.getInstance().createQuery(
          PriceListVersion.class, whereClause);
      ppriceListVersion.setNamedParameter("priceList", priceList);
      ppriceListVersion.setNamedParameter("clientId", clientId);

      if (!ppriceListVersion.list().isEmpty()) {
        return ppriceListVersion.list().get(0).getId();
      } else {
        return "0";
      }
    } catch (Exception e) {
      throw new OBException(e);
    }
  }

  private BigDecimal getPriceList(String strProductID, String strPriceVersionId) {
    BigDecimal bdPriceList = null;
    try {
      final List<Object> parameters = new ArrayList<Object>();
      parameters.add(strProductID);
      parameters.add(strPriceVersionId);
      final String procedureName = "M_BOM_PriceList";
      bdPriceList = (BigDecimal) CallStoredProcedure.getInstance().call(procedureName, parameters,
          null);
    } catch (Exception e) {
      throw new OBException(e);
    }

    return (bdPriceList);
  }

}
